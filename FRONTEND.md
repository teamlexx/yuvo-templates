# FrontEnd Files

### CSS
```html
<!-- animate.min -->
https://bbcdn.githack.com/teamlexx/yuvo-templates/raw/06c22b9f930bfd0c3d9e0721ad2deaa989d27048/frontend/css/animate.min.css

<!-- common-styles -->
https://bbcdn.githack.com/teamlexx/yuvo-templates/raw/master/frontend/css/common-styles.css

<!-- hover -->
https://bbcdn.githack.com/teamlexx/yuvo-templates/raw/06c22b9f930bfd0c3d9e0721ad2deaa989d27048/frontend/css/hover.css

```

### JS
```html
<!-- common-scripts -->
https://bbcdn.githack.com/teamlexx/yuvo-templates/raw/master/frontend/js/common-scripts.js

<!-- jquery cookie CDN -->
https://cdnjs.cloudflare.com/ajax/libs/jquery-cookie/1.4.1/jquery.cookie.min.js

<!-- modernize -->
https://bbcdn.githack.com/teamlexx/yuvo-templates/raw/06c22b9f930bfd0c3d9e0721ad2deaa989d27048/frontend/js/modernizr.js


```

