# Plugins used by the system
If they are hosted by CDN, they won't be included here unless there are local modifications done on them.

```html
<!-- accounting js -->
https://cdnjs.cloudflare.com/ajax/libs/accounting.js/0.4.1/accounting.min.js

<!-- animate css -->
https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.5.2/animate.min.css

<!-- autosize -->
https://cdnjs.cloudflare.com/ajax/libs/autosize.js/4.0.0/autosize.min.js

<!-- axios -->
https://cdnjs.cloudflare.com/ajax/libs/axios/0.17.1/axios.min.js

<!-- Bootstrap -->
https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css
https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js
https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.bundle.min.js


https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css
https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js

<!-- bootstrap material datetime picker -->
https://cdnjs.cloudflare.com/ajax/libs/bootstrap-material-datetimepicker/2.7.1/css/bootstrap-material-datetimepicker.min.css

https://cdnjs.cloudflare.com/ajax/libs/bootstrap-material-datetimepicker/2.7.1/css/bootstrap-material-datetimepicker.min.css.map

https://cdnjs.cloudflare.com/ajax/libs/bootstrap-material-datetimepicker/2.7.1/js/bootstrap-material-datetimepicker.min.js

https://cdnjs.cloudflare.com/ajax/libs/bootstrap-material-datetimepicker/2.7.1/js/bootstrap-material-datetimepicker.min.js.map

<!-- bootstrap select -->
https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.4/css/bootstrap-select.min.css
https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.4/js/bootstrap-select.min.js

<!-- bootstrap multiselect -->
https://cdnjs.cloudflare.com/ajax/libs/bootstrap-multiselect/0.9.13/css/bootstrap-multiselect.css

https://cdnjs.cloudflare.com/ajax/libs/bootstrap-multiselect/0.9.13/js/bootstrap-multiselect.min.js

<!-- bootstrap notify -->
https://cdnjs.cloudflare.com/ajax/libs/bootstrap-notify/0.2.0/css/bootstrap-notify.min.css
https://cdnjs.cloudflare.com/ajax/libs/bootstrap-notify/0.2.0/css/styles/alert-bangtidy.min.css
https://cdnjs.cloudflare.com/ajax/libs/bootstrap-notify/0.2.0/css/styles/alert-blackgloss.min.css

https://cdnjs.cloudflare.com/ajax/libs/bootstrap-notify/0.2.0/js/bootstrap-notify.min.js

<!-- bootstrap select -->
https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.4/css/bootstrap-select.min.css
https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.4/js/bootstrap-select.min.js

<!-- bootstrap strength meter -->
https://cdnjs.cloudflare.com/ajax/libs/pwstrength-bootstrap/2.1.1/pwstrength-bootstrap.min.js

<!-- bootstrap switch -->
https://cdnjs.cloudflare.com/ajax/libs/bootstrap-switch/3.3.4/css/bootstrap3/bootstrap-switch.min.css
https://cdnjs.cloudflare.com/ajax/libs/bootstrap-switch/3.3.4/js/bootstrap-switch.min.js

<!-- bootstrap tagsinput -->
https://cdnjs.cloudflare.com/ajax/libs/bootstrap-tagsinput/0.8.0/bootstrap-tagsinput.css
https://cdnjs.cloudflare.com/ajax/libs/bootstrap-tagsinput/0.8.0/bootstrap-tagsinput.min.js

<!-- bootstrap touchspin -->
https://cdnjs.cloudflare.com/ajax/libs/bootstrap-touchspin/3.1.2/jquery.bootstrap-touchspin.min.css
https://cdnjs.cloudflare.com/ajax/libs/bootstrap-touchspin/3.1.2/jquery.bootstrap-touchspin.min.js

<!-- britecharts -->
https://cdnjs.cloudflare.com/ajax/libs/britecharts/3.0.0/css/britecharts.min.css
https://cdnjs.cloudflare.com/ajax/libs/britecharts/3.0.0/css/common/common.min.css
https://cdnjs.cloudflare.com/ajax/libs/britecharts/3.0.0/bundled/britecharts.min.js

<!-- chartjs -->
https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.7.1/Chart.bundle.min.js
https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.7.1/Chart.min.js

<!-- ckeditor -->
https://cdnjs.cloudflare.com/ajax/libs/ckeditor/4.8.0/ckeditor.js

<!-- clipboard js -->
https://cdnjs.cloudflare.com/ajax/libs/clipboard.js/1.7.1/clipboard.min.js

<!-- counter-up -->
https://cdnjs.cloudflare.com/ajax/libs/Counter-Up/1.0.0/jquery.counterup.min.js

<!-- custom file inputs -->
https://bbcdn.githack.com/teamlexx/yuvo-templates/raw/0e7e352aeb08f0297420bc12d6720009d8db40da/plugins/CustomFileInputs/css/component.css

https://bbcdn.githack.com/teamlexx/yuvo-templates/raw/0e7e352aeb08f0297420bc12d6720009d8db40da/plugins/CustomFileInputs/js/custom-file-input.js

https://bbcdn.githack.com/teamlexx/yuvo-templates/raw/0e7e352aeb08f0297420bc12d6720009d8db40da/plugins/CustomFileInputs/js/jquery.custom-file-input.js

<!-- datatables -->
<!-- for bootstrap 3 -->
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/bs/jszip-2.5.0/dt-1.10.16/af-2.2.2/b-1.5.1/b-colvis-1.5.1/b-flash-1.5.1/b-html5-1.5.1/b-print-1.5.1/fc-3.2.4/fh-3.1.3/kt-2.3.2/r-2.2.1/rg-1.0.2/rr-1.2.3/sc-1.4.3/sl-1.2.4/datatables.min.css"/>
 
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/pdfmake.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/vfs_fonts.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/v/bs/jszip-2.5.0/dt-1.10.16/af-2.2.2/b-1.5.1/b-colvis-1.5.1/b-flash-1.5.1/b-html5-1.5.1/b-print-1.5.1/fc-3.2.4/fh-3.1.3/kt-2.3.2/r-2.2.1/rg-1.0.2/rr-1.2.3/sc-1.4.3/sl-1.2.4/datatables.min.js"></script>

<!-- for bootstrap 4 -->
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/bs4/jszip-2.5.0/dt-1.10.16/af-2.2.2/b-1.5.1/b-colvis-1.5.1/b-flash-1.5.1/b-html5-1.5.1/b-print-1.5.1/fc-3.2.4/fh-3.1.3/kt-2.3.2/r-2.2.1/rg-1.0.2/rr-1.2.3/sc-1.4.3/sl-1.2.4/datatables.min.css"/>
 
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/pdfmake.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/vfs_fonts.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/v/bs4/jszip-2.5.0/dt-1.10.16/af-2.2.2/b-1.5.1/b-colvis-1.5.1/b-flash-1.5.1/b-html5-1.5.1/b-print-1.5.1/fc-3.2.4/fh-3.1.3/kt-2.3.2/r-2.2.1/rg-1.0.2/rr-1.2.3/sc-1.4.3/sl-1.2.4/datatables.min.js"></script>

<!-- dropzone -->
https://cdnjs.cloudflare.com/ajax/libs/dropzone/5.2.0/min/dropzone.min.css
https://cdnjs.cloudflare.com/ajax/libs/dropzone/5.2.0/min/dropzone.min.js

<!-- easy-pie-chart -->
https://cdnjs.cloudflare.com/ajax/libs/easy-pie-chart/2.1.6/easypiechart.min.js
https://cdnjs.cloudflare.com/ajax/libs/easy-pie-chart/2.1.6/jquery.easypiechart.min.js

<!-- easy piecharts -->
https://cdnjs.cloudflare.com/ajax/libs/easy-pie-chart/2.1.6/easypiechart.min.js

<!-- editable table -->
https://bbcdn.githack.com/teamlexx/yuvo-templates/raw/0e7e352aeb08f0297420bc12d6720009d8db40da/plugins/editable-table/mindmup-editabletable.js

<!-- fancybox -->
https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.2.5/jquery.fancybox.min.css
https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.2.5/jquery.fancybox.min.js

<!-- font awesome -->
https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css

<!-- intl tel input -->
https://cdnjs.cloudflare.com/ajax/libs/intl-tel-input/12.1.8/css/intlTelInput.css
https://cdnjs.cloudflare.com/ajax/libs/intl-tel-input/12.1.8/img/flags.png
https://cdnjs.cloudflare.com/ajax/libs/intl-tel-input/12.1.8/img/flags@2x.png
https://cdnjs.cloudflare.com/ajax/libs/intl-tel-input/12.1.8/js/intlTelInput.min.js
https://cdnjs.cloudflare.com/ajax/libs/intl-tel-input/12.1.8/js/utils.js

<!-- isotope -->
https://cdnjs.cloudflare.com/ajax/libs/jquery.isotope/3.0.5/isotope.pkgd.min.js

<!-- jarallax -->
https://cdnjs.cloudflare.com/ajax/libs/jarallax/1.9.3/jarallax.min.js

<!-- jquery -->
https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js
https://code.jquery.com/jquery-2.2.4.min.js
https://code.jquery.com/jquery-1.12.4.min.js

<!-- jquery UI -->
https://code.jquery.com/ui/1.12.1/jquery-ui.min.js
https://code.jquery.com/ui/1.12.1/themes/smoothness/jquery-ui.css
https://code.jquery.com/ui/1.12.1/themes/ui-darkness/jquery-ui.css
https://code.jquery.com/ui/1.12.1/themes/cupertino/jquery-ui.css

<!-- jquery mobile -->
https://code.jquery.com/mobile/1.4.5/jquery.mobile-1.4.5.min.js

<!-- jquery cookie -->
https://cdnjs.cloudflare.com/ajax/libs/jquery-cookie/1.4.1/jquery.cookie.min.js

<!-- jquery countdown -->
https://cdnjs.cloudflare.com/ajax/libs/jquery.countdown/2.2.0/jquery.countdown.min.js

<!-- jquery count-to -->
https://cdnjs.cloudflare.com/ajax/libs/jquery-countto/1.2.0/jquery.countTo.min.js

<!-- jquery.blockUI -->
https://cdnjs.cloudflare.com/ajax/libs/jquery.blockUI/2.70/jquery.blockUI.min.js

<!-- jquery datetime-picker -->
https://cdnjs.cloudflare.com/ajax/libs/jquery-datetimepicker/2.5.17/jquery.datetimepicker.min.css
https://cdnjs.cloudflare.com/ajax/libs/jquery-datetimepicker/2.5.17/jquery.datetimepicker.full.min.js

<!-- jquery freeze table-rows -->
https://bbcdn.githack.com/teamlexx/yuvo-templates/raw/0e7e352aeb08f0297420bc12d6720009d8db40da/plugins/jquery-freeze-table-rows-columns/tableHeadFixer.js

<!-- jquery mCustomScrollbar -->
https://cdnjs.cloudflare.com/ajax/libs/malihu-custom-scrollbar-plugin/3.1.5/jquery.mCustomScrollbar.min.css
https://cdnjs.cloudflare.com/ajax/libs/malihu-custom-scrollbar-plugin/3.1.5/jquery.mCustomScrollbar.min.js
https://cdnjs.cloudflare.com/ajax/libs/malihu-custom-scrollbar-plugin/3.1.5/jquery.mCustomScrollbar.concat.min.js

<!-- jquery slimscroll -->
https://cdnjs.cloudflare.com/ajax/libs/jQuery-slimScroll/1.3.8/jquery.slimscroll.min.js

<!-- jquery.ui.touch-punch -->
https://cdnjs.cloudflare.com/ajax/libs/jqueryui-touch-punch/0.2.3/jquery.ui.touch-punch.min.js

<!-- jquery tagsinput -->
https://cdnjs.cloudflare.com/ajax/libs/jquery-tagsinput/1.3.6/jquery.tagsinput.min.css
https://cdnjs.cloudflare.com/ajax/libs/jquery-tagsinput/1.3.6/jquery.tagsinput.min.js

<!-- jquery touchswipe -->
https://cdnjs.cloudflare.com/ajax/libs/jquery.touchswipe/1.6.18/jquery.touchSwipe.min.js

<!-- jquery viewportchecker -->
https://cdnjs.cloudflare.com/ajax/libs/jQuery-viewport-checker/1.8.8/jquery.viewportchecker.min.js

<!-- jspdf -->
https://cdnjs.cloudflare.com/ajax/libs/jspdf/1.3.5/jspdf.min.js

<!-- jtable -->
https://bbcdn.githack.com/teamlexx/yuvo-templates/raw/0e7e352aeb08f0297420bc12d6720009d8db40da/plugins/jtable/jquery-ui-1.10.4.custom.css

https://bbcdn.githack.com/teamlexx/yuvo-templates/raw/0e7e352aeb08f0297420bc12d6720009d8db40da/plugins/jtable/jquery-ui-1.10.4.min.js

https://bbcdn.githack.com/teamlexx/yuvo-templates/raw/0e7e352aeb08f0297420bc12d6720009d8db40da/plugins/jtable/jquery.jtable.min.js

<!-- login-signup-modal window -->
https://bbcdn.githack.com/teamlexx/yuvo-templates/raw/0e7e352aeb08f0297420bc12d6720009d8db40da/plugins/login-signup-modal-window/css/login-signup-modal-no-navbar.css

https://bbcdn.githack.com/teamlexx/yuvo-templates/raw/0e7e352aeb08f0297420bc12d6720009d8db40da/plugins/login-signup-modal-window/js/modernizr.js

https://bbcdn.githack.com/teamlexx/yuvo-templates/raw/0e7e352aeb08f0297420bc12d6720009d8db40da/plugins/login-signup-modal-window/js/main.js

<!-- momentjs -->
https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.20.1/moment.min.js
https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.20.1/moment-with-locales.min.js

<!-- multi-select -->
https://cdnjs.cloudflare.com/ajax/libs/multi-select/0.9.12/css/multi-select.min.css
https://cdnjs.cloudflare.com/ajax/libs/multi-select/0.9.12/js/jquery.multi-select.min.js

<!-- multifile -->
https://bbcdn.githack.com/teamlexx/yuvo-templates/raw/0e7e352aeb08f0297420bc12d6720009d8db40da/plugins/multifile/jQuery.MultiFile.min.js

<!-- nestable -->
https://bbcdn.githack.com/teamlexx/yuvo-templates/raw/0e7e352aeb08f0297420bc12d6720009d8db40da/plugins/nestable/jquery-nestable.css

https://bbcdn.githack.com/teamlexx/yuvo-templates/raw/0e7e352aeb08f0297420bc12d6720009d8db40da/plugins/nestable/jquery.nestable.js


<!-- owlcarousel 2 -->
https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.2.1/assets/ajax-loader.gif
https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.2.1/assets/owl.carousel.min.css
https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.2.1/assets/owl.theme.default.min.css
https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.2.1/assets/owl.theme.green.min.css
https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.2.1/assets/owl.video.play.png
https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.2.1/owl.carousel.min.js

<!-- pacejs -->
<!-- available colors; black, blue, green, orange, pink, purple, red, silver, white, yellow-->
https://cdnjs.cloudflare.com/ajax/libs/pace/1.0.2/pace.min.js
https://cdnjs.cloudflare.com/ajax/libs/pace/1.0.2/themes/black/pace-theme-barber-shop.min.css
https://cdnjs.cloudflare.com/ajax/libs/pace/1.0.2/themes/black/pace-theme-big-counter.min.css
https://cdnjs.cloudflare.com/ajax/libs/pace/1.0.2/themes/black/pace-theme-bounce.min.css
https://cdnjs.cloudflare.com/ajax/libs/pace/1.0.2/themes/black/pace-theme-center-atom.min.css
https://cdnjs.cloudflare.com/ajax/libs/pace/1.0.2/themes/black/pace-theme-center-circle.min.css
https://cdnjs.cloudflare.com/ajax/libs/pace/1.0.2/themes/black/pace-theme-center-radar.min.css
https://cdnjs.cloudflare.com/ajax/libs/pace/1.0.2/themes/black/pace-theme-center-simple.min.css
https://cdnjs.cloudflare.com/ajax/libs/pace/1.0.2/themes/black/pace-theme-corner-indicator.min.css
https://cdnjs.cloudflare.com/ajax/libs/pace/1.0.2/themes/black/pace-theme-fill-left.min.css
https://cdnjs.cloudflare.com/ajax/libs/pace/1.0.2/themes/black/pace-theme-flash.min.css
https://cdnjs.cloudflare.com/ajax/libs/pace/1.0.2/themes/black/pace-theme-flat-top.min.css
https://cdnjs.cloudflare.com/ajax/libs/pace/1.0.2/themes/black/pace-theme-loading-bar.min.css
https://cdnjs.cloudflare.com/ajax/libs/pace/1.0.2/themes/black/pace-theme-loading-bar.min.css
https://cdnjs.cloudflare.com/ajax/libs/pace/1.0.2/themes/black/pace-theme-mac-osx.min.css
https://cdnjs.cloudflare.com/ajax/libs/pace/1.0.2/themes/black/pace-theme-minimal.min.css

<!-- prettyPhoto -->
https://cdnjs.cloudflare.com/ajax/libs/prettyPhoto/3.1.6/css/prettyPhoto.min.css
https://cdnjs.cloudflare.com/ajax/libs/prettyPhoto/3.1.6/js/jquery.prettyPhoto.min.js

<!-- prettify -->
https://cdnjs.cloudflare.com/ajax/libs/prettify/r298/prettify.min.css
https://cdnjs.cloudflare.com/ajax/libs/prettify/r298/prettify.min.js

<!-- pdfjs -->
https://cdnjs.cloudflare.com/ajax/libs/pdf.js/2.0.305/pdf.combined.js

<!-- pnotify -->
https://cdnjs.cloudflare.com/ajax/libs/pnotify/3.2.1/pnotify.css
https://cdnjs.cloudflare.com/ajax/libs/pnotify/3.2.1/pnotify.brighttheme.css
https://cdnjs.cloudflare.com/ajax/libs/pnotify/3.2.1/pnotify.buttons.css
https://cdnjs.cloudflare.com/ajax/libs/pnotify/3.2.1/pnotify.js
https://cdnjs.cloudflare.com/ajax/libs/pnotify/3.2.1/pnotify.animate.js
https://cdnjs.cloudflare.com/ajax/libs/pnotify/3.2.1/pnotify.confirm.js
https://cdnjs.cloudflare.com/ajax/libs/pnotify/3.2.1/pnotify.desktop.js

<!-- selectize -->
https://cdnjs.cloudflare.com/ajax/libs/selectize.js/0.12.4/css/selectize.css
https://cdnjs.cloudflare.com/ajax/libs/selectize.js/0.12.4/css/selectize.bootstrap3.min.css
https://cdnjs.cloudflare.com/ajax/libs/selectize.js/0.12.4/css/selectize.default.min.css
https://cdnjs.cloudflare.com/ajax/libs/selectize.js/0.12.4/css/selectize.min.css
https://cdnjs.cloudflare.com/ajax/libs/selectize.js/0.12.4/js/standalone/selectize.min.js

<!-- smoothscroll -->
https://cdnjs.cloudflare.com/ajax/libs/smooth-scroll/12.1.5/js/smooth-scroll.min.js

<!-- sweetalert -->
https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.css
https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.js

<!-- tether -->
https://cdnjs.cloudflare.com/ajax/libs/tether/1.4.3/css/tether.min.css
https://cdnjs.cloudflare.com/ajax/libs/tether/1.4.3/js/tether.min.js
https://cdnjs.cloudflare.com/ajax/libs/tether/1.4.3/css/tether-theme-basic.min.css
https://cdnjs.cloudflare.com/ajax/libs/tether/1.4.3/css/tether-theme-arrows-dark.min.css
https://cdnjs.cloudflare.com/ajax/libs/tether/1.4.3/css/tether-theme-arrows.min.css

<!-- tinymce -->
https://cdn.jsdelivr.net/npm/tinymce@4.7.6/tinymce.min.js

<!-- toastr -->
https://cdnjs.cloudflare.com/ajax/libs/toastr.js/2.1.4/toastr.min.css
https://cdnjs.cloudflare.com/ajax/libs/toastr.js/2.1.4/toastr.min.js

<!-- tooltipster -->
https://cdnjs.cloudflare.com/ajax/libs/tooltipster/3.3.0/css/tooltipster.min.css
https://cdnjs.cloudflare.com/ajax/libs/tooltipster/3.3.0/css/themes/tooltipster-light.min.css
https://cdnjs.cloudflare.com/ajax/libs/tooltipster/3.3.0/css/themes/tooltipster-noir.min.css
https://cdnjs.cloudflare.com/ajax/libs/tooltipster/3.3.0/css/themes/tooltipster-shadow.min.css
https://cdnjs.cloudflare.com/ajax/libs/tooltipster/3.3.0/js/jquery.tooltipster.min.js

<!-- twitter-bootstrap-wizard -->
https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap-wizard/1.2/jquery.bootstrap.wizard.min.js

<!-- waitme -->
https://bbcdn.githack.com/teamlexx/yuvo-templates/raw/0e7e352aeb08f0297420bc12d6720009d8db40da/plugins/waitme/waitMe.min.css

https://bbcdn.githack.com/teamlexx/yuvo-templates/raw/0e7e352aeb08f0297420bc12d6720009d8db40da/plugins/waitme/waitMe.min.js

<!-- pleasewait -->
https://cdnjs.cloudflare.com/ajax/libs/please-wait/0.0.5/please-wait.min.css
https://cdnjs.cloudflare.com/ajax/libs/please-wait/0.0.5/please-wait.min.js

<!-- wow -->
https://cdnjs.cloudflare.com/ajax/libs/wow/1.1.2/wow.min.js

```