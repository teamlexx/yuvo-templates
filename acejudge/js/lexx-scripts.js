/*
Customization Script by: Lexx YungCarter
Github: Lexx-YungCarter
Twitter: @UnderscoreLexx
Google+: google.com/+LexxYungCarter
==========================
This script is beautifully made with passion based on the modern approaches of JavaScript
Technologies. The items outlined here pose some critical UI and UX design patterns that should 
not be edited (unless of course, under supervision or parental guidance :-D).
Feel free to copy and paste to your projects, but on condition that you credit the author 
(of course Me, daah!)

***HAPPY CODING***

*/
$(document).ready(function () {
    // handle page scroll
    $(".scroll").click(function (event) {
        event.preventDefault();

        $('html,body').animate({
            scrollTop: $(this.hash).offset().top
        }, 1000);
    });

    $().UItoTop({ easingType: 'easeOutQuart' });

    // menu
    $('.navicon').on('click', function (e) {
        e.preventDefault();
        $(this).toggleClass('navicon--active');
        $('.toggle').toggleClass('toggle--active');
    });

    // flexslider
    if ($('.flexslider').length) {
        $('.flexslider').flexslider({
            animation: "slide",
            start: function (slider) {
                $('body').removeClass('loading');
            }
        });
    }

    // handle parallax effect
    if ($('.flexslider').length) {
        $('.jarallax').jarallax({
            speed: 0.5,
            imgWidth: 1366,
            imgHeight: 768
        })
    }
    
});
