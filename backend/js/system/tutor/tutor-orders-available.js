$(document).ready(function() {
    var fetch_url = $('#dataTables-available-orders').data('fetch-url');
    var _token = $('meta[name="csrf-token"]').attr('content');

    var table = $('#dataTables-available-orders').DataTable({
        "order": [ 1, "desc" ],
        paging: false,
        info: false,
        processing: true,
        serverSide: true,
        ajax: {
            "url" : fetch_url,
            "type": "POST",
            "data": {
                _token: _token
            },
        },
        columns: [
            {
                "className": 'details-control',
                "orderable": false,
                "data": null,
                "defaultContent": '<strong><i class="fa fa-chevron-circle-down text-center"></i></strong>'
            },
            { data: 'custom_id', name: 'custom_id' },
            { data: 'topic', name: 'topic' },
            { data: 'field', name: 'field' },
            { data: 'pages', name: 'pages'  },
            { data: 'proposed_tutor_deadline', name: 'proposed_tutor_deadline' },
            { data: 'amount', name: 'amount', 'orderable' : false, 'searchable': false }
        ],
        columnDefs: [
            { width: '70px', targets: 1 },
            { width: '150px', targets: 3 },
            // { width: '20px', targets: 4 },
            { width: '90px', targets: 6 },
        ],
        rowCallback: function (nRow) {
            $(nRow).find('[data-countdown]').each(function () {
                var $this = $(this),
                    countdown_date = $(this).data('countdown');
                $this.countdown(countdown_date, { elapse: true })
                    .on('update.countdown', function (event) {
                        if (event.elapsed) {
                            // after end - time elapsed
                            $this.text(event.strftime('%D days %Hh %Mmin'));
                        } else {
                            // to end - time not elapsed
                            $this.text(event.strftime('%D days %Hh %Mmin'));
                        }
                    });
            });
        }
    });

    // add event listener for opening and closing details
    $('#dataTables-available-orders tbody').on('click', 'tr', function() {
        // console.log('clicked');
        var tr = $(this);
        var row = table.row(tr);

        if(row.child.isShown()) {
            // this row is already open - close it
            row.child.hide();
            tr.removeClass('shown');
        }
        else {
            // open this row
            row.child( format_child_row(row.data()) ).show();
            tr.addClass('shown');

        }
    });
    
});

// formatting function for row details
function format_child_row(d) {
    // /d is he original data object for the row
    return '<div class="table-detail">' +
                '<div class="row">' +
                    '<div class="col-xs-6">' +
                        // left column entries
                        '<div class="row">' +
                            '<div class="col-xs-3 text-right">' +
                                '<p>#:</p>' +
                            '</div>' +
                            '<div class="col-xs-9 text-left">' +
                                '<p>' + d.custom_id  + '</p>' +
                            '</div>' +
                            '<div class="clearfix"></div>' + // clearfix

                            '<div class="col-xs-3 text-right">' +
                                '<p>Topic:</p>' +
                            '</div>' +
                            '<div class="col-xs-9 text-left">' +
                                '<p>' + d.topic + '</p>' +
                            '</div>' +
                            '<div class="clearfix"></div>' + // clearfix
                            
                            '<div class="col-xs-3 text-right">' +
                                '<p>Field:</p>' +
                            '</div>' +
                            '<div class="col-xs-9 text-left">' +
                                '<p>' + d.field + '</p>' +
                            '</div>' +
                            '<div class="clearfix"></div>' + // clearfix
                            
                            '<div class="col-xs-4 text-right">' +
                                '<p>Order Totals:</p>' +
                            '</div>' +
                            '<div class="col-xs-8 text-left">' +
                                '<p>' + d.amount + '</p>' +
                            '</div>' +
                            '<div class="clearfix"></div>' + // clearfix

                            '<div class="col-xs-3 text-right">' +
                                '<p>Deadline:</p>' +
                            '</div>' +
                            '<div class="col-xs-9 text-left">' +
                                '<p>' + d.proposed_tutor_deadline + '</p>' +
                            '</div>' +
                            '<div class="clearfix"></div>' + // clearfix

                        '</div>' +
                    '</div>' + // end of column 2

                    // right column entries
                    '<div class="col-xs-6">' +
                        '<div class="row">' +
                            '<div class="col-xs-4 text-right">' +
                                '<p># of Pages:</p>' +
                            '</div>' +
                            '<div class="col-xs-8 text-left">' +
                                '<p>' + d.number_of_pages + '</p>' +
                            '</div>' +
                            '<div class="clearfix"></div>' + // clearfix
                            
                            '<div class="col-xs-4 text-right">' +
                                '<p># of Slides:</p>' +
                            '</div>' +
                            '<div class="col-xs-8 text-left">' +
                                '<p>' + d.number_of_slides + '</p>' +
                            '</div>' +
                            '<div class="clearfix"></div>' + // clearfix

                            '<div class="col-xs-4 text-right">' +
                                '<p># of Charts:</p>' +
                            '</div>' +
                            '<div class="col-xs-8 text-left">' +
                                '<p>' + d.number_of_charts + '</p>' +
                            '</div>' +
                            '<div class="clearfix"></div>' + // clearfix

                            '<div class="col-xs-4 text-right">' +
                                '<p># of Files:</p>' +
                            '</div>' +
                            '<div class="col-xs-8 text-left">' +
                                '<p>' + d.number_of_files + '</p>' +
                            '</div>' +
                            '<div class="clearfix"></div>' + // clearfix
                            
                            '<div class="col-xs-12">' +
                                '<p class="text-bold text-muted">Actions</p>' +
                            '</div>' +
                            '<div class="col-xs-12">' +
                                '<div>' + d.actions + '</div>' +
                            '</div>' +
                            '<div class="clearfix"></div>' + // clearfix

                        '</div>' + // end of row inside column 2
                    '</div>' + // end of column 2
                
                '</div>' + // end of row
            '</div>'; // end of table-details
}