$(document).ready(function() {
    var fetch_url = $('#dataTables-bids-unconfirmed').data('fetch-url');
    var _token = $('meta[name="csrf-token"]').attr('content');

    var table = $('#dataTables-bids-unconfirmed').DataTable({
        "order": [1, "desc"],
        searching: true,
        paging: true,
        info: true,
        processing: false,
        serverSide: true,
        ajax: {
            "url": fetch_url,
            "type": "POST",
            "data": {
                _token: _token
            },
        },
        columns: [
            {
                "className": 'details-control',
                "orderable": false,
                "data": null,
                "defaultContent": '<strong><i class="fa fa-chevron-circle-down text-center"></i></strong>'
            },
            { data: 'custom_id', name: 'custom_id' },
            { data: 'field', name: 'field' },
            { data: 'pages', name: 'pages' },
            { data: 'bid_deadline', name: 'bid_deadline' },
            { data: 'bid_amount', name: 'bid_amount', 'orderable': false, 'searchable': false }
        ],
        columnDefs: [
            { width: '70px', targets: 1 },
            { width: '150px', targets: 2 },
            // { width: '20px', targets: 4 },
            { width: '150px', targets: 4 },
            { width: '150px', targets: 5 },
        ]
    });

    // add event listener for opening and closing details
    $('#dataTables-bids-unconfirmed tbody').on('click', 'tr', function() {
        // console.log('clicked');
        var tr = $(this);
        var row = table.row(tr);

        if (row.child.isShown()) {
            // this row is already open - close it
            row.child.hide();
            tr.removeClass('shown');
        } else {
            // open this row
            row.child(format_child_row(row.data())).show();
            tr.addClass('shown');

        }
    });

});

// formatting function for row details
function format_child_row(d) {
    // /d is he original data object for the row
    return '<div class="table-detail">' +
        '<div class="row">' +
        '<div class="col-xs-6">' +
        // left column entries
        '<div class="row">' +
        '<div class="col-xs-3 text-right">' +
        '<p>#:</p>' +
        '</div>' +
        '<div class="col-xs-9 text-left">' +
        '<p>' + d.order_id + '</p>' +
        '</div>' +
        '<div class="clearfix"></div>' + // clearfix

        '<div class="col-xs-3 text-right">' +
        '<p>Topic:</p>' +
        '</div>' +
        '<div class="col-xs-9 text-left">' +
        '<p>' + d.order.topic + '</p>' +
        '</div>' +
        '<div class="clearfix"></div>' + // clearfix

        '<div class="col-xs-3 text-right">' +
        '<p>Field:</p>' +
        '</div>' +
        '<div class="col-xs-9 text-left">' +
        '<p>' + d.field + '</p>' +
        '</div>' +
        '<div class="clearfix"></div>' + // clearfix

        '<div class="col-xs-5 text-right">' +
        '<p># of Pages:</p>' +
        '</div>' +
        '<div class="col-xs-7 text-left">' +
        '<p>' + d.order.number_of_pages + '</p>' +
        '</div>' +
        '<div class="clearfix"></div>' + // clearfix

        '</div>' +
        '</div>' + // end of column 2

        // right column entries
        '<div class="col-xs-6">' +

        '<div class="row">' +

        '<div class="col-xs-5 text-right">' +
        '<p># of Slides:</p>' +
        '</div>' +
        '<div class="col-xs-7 text-left">' +
        '<p>' + d.order.number_of_slides + '</p>' +
        '</div>' +
        '<div class="clearfix"></div>' + // clearfix

        '<div class="col-xs-5 text-right">' +
        '<p># of Charts:</p>' +
        '</div>' +
        '<div class="col-xs-7 text-left">' +
        '<p>' + d.order.number_of_charts + '</p>' +
        '</div>' +
        '<div class="clearfix"></div>' + // clearfix

        '<div class="col-xs-5 text-right">' +
        '<p># of Files:</p>' +
        '</div>' +
        '<div class="col-xs-7 text-left">' +
        '<p>' + d.number_of_files + '</p>' +
        '</div>' +
        '<div class="clearfix"></div>' + // clearfix

        '<div class="col-xs-12">' +
        '<p class="text-bold text-muted">Actions</p>' +
        '</div>' +
        '<div class="col-xs-12">' +
        '<div>' + d.actions + '</div>' +
        '</div>' +
        '<div class="clearfix"></div>' + // clearfix

        '</div>' +
        '</div>' + // end of column 2

        '</div>' + // end of row
        '</div>'; // end of table-details
}