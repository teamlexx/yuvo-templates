$(document).ready(function() {
    var available_orders_span = $('.available-orders-span');
    var bids_orders_span = $('.bids-orders-span');
    var bids_unconfirmed_orders_span = $('.bids-unconfirmed-orders-span');
    var current_orders_span = $('.current-orders-span');
    var done_orders_span = $('.done-orders-span');
    var revision_orders_span = $('.revision-orders-span');
    var confirmed_orders_span = $('.confirmed-orders-span');
    var rejected_orders_span = $('.rejected-orders-span');
    var disputed_orders_span = $('.disputed-orders-span');

    var fetch_url = '/tutor/getOrderTotals';
    var _token = $('meta[name="csrf-token"]').attr('content');

    $.ajax({
        url: fetch_url,
        type: 'POST',
        data: {
            _token: _token
        },
        success: function(data) {
            available_orders_span.text(data.available_orders);
            bids_orders_span.text(data.bids_orders);
            bids_unconfirmed_orders_span.text(data.bids_unconfirmed_orders);
            current_orders_span.text(data.current_orders);
            done_orders_span.text(data.done_orders);
            revision_orders_span.text(data.revision_orders);
            confirmed_orders_span.text(data.confirmed_orders);
            rejected_orders_span.text(data.rejected_orders);
            disputed_orders_span.text(data.disputed_orders);
        }
    });
    
});
