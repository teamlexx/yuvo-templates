jQuery(document).ready(function() {
    var all_editors_span = $('.all-editors-span');
    var active_editors_span = $('.active-editors-span');
    var inactive_editors_span = $('.inactive-editors-span');
    var deactivated_editors_span = $('.deactivated-editors-span');

    var fetch_url = $('#admin-get-editors-totals').data('route'); 
    var _token = $('meta[name="csrf-token"]').attr('content');

    jQuery.ajax({
        url: fetch_url,
        method: 'POST',
        data: {
            _token: _token
        },
        success: function(data) {
            // console.log(data);
            all_editors_span.text(data.all_editors);
            active_editors_span.text(data.active_editors_span);
            inactive_editors_span.text(data.inactive_editors_span);
            deactivated_editors_span.text(data.deactivated_editors_span);
        }
    });
    
});
