$(document).ready(function() {
    var _token = $('meta[name="csrf-token"]').attr('content');

    $('#form-make-payments').submit(function(e) {
        e.preventDefault();
        check_user_payments(
            $(this), 
            $('#user-payments-table tbody'), 
            $('#user-payments-table').data('order-url')
        );
        
    });

    // check all button
    $('#check_all').on('click', function() {
        var is_checked = $(this).prop('checked');
        // console.log(is_checked);

        if(is_checked) {
            // check all the payments checkboxes
            $('#user-payments-table tbody').find('input[type="checkbox"]').prop('checked', true);
        } else {
            $('#user-payments-table tbody').find('input[type="checkbox"]').prop('checked', false);
        }
    });

    // payments-matured-form submit
    $('#payments-matured-form').submit(function(e) {
        e.preventDefault();
        var checked_payments = [];
        $.each($('#user-payments-table tbody input[type="checkbox"]:checked'), function(index, value) {
            checked_payments.push($(this).val());
        });

        var fetch_url = $(this).attr('action');
        var user_id = $('#form-make-payments select#user_id').val();
        // console.log(fetch_url);

        $.ajax({
            url: fetch_url,
            type: 'post',
            data: {
                _token: _token,
                checked_payments: checked_payments,
                user_id: user_id
            },
            success: function(data) {
                // console.log(data);
                if(data.status == "OK") {
                    toastr.success('Payments successfully marked as matured for withdrawal. The respective writer will be alerted');
                    setTimeout(function() {
                        // window.location.reload(true);
                        check_user_payments(
                            $('#form-make-payments'), 
                            $('#user-payments-table tbody'), 
                            $('#user-payments-table').data('order-url')
                        );
                    }, 2000);

                }
            }
        });
    });
    
    
    // payments-paid-form submit
    $('#payments-paid-form').submit(function(e) {
        e.preventDefault();
        var checked_payments = [];
        $.each($('#user-payments-table tbody input[type="checkbox"]:checked'), function(index, value) {
            checked_payments.push($(this).val());
        });

        var fetch_url = $(this).attr('action');
        var user_id = $('#form-make-payments select#user_id').val();
        console.log(fetch_url);

        $.ajax({
            url: fetch_url,
            type: 'post',
            data: {
                _token: _token,
                checked_payments: checked_payments,
                user_id: user_id
            },
            success: function(data) {
                // console.log(data);
                if(data.status == "OK") {
                    toastr.success('Payments successfully marked as Paid to Writer/Tutor. The respective writer will be alerted');
                    setTimeout(function() {
                        // window.location.reload(true);
                        check_user_payments(
                            $('#form-make-payments'), 
                            $('#user-payments-table tbody'), 
                            $('#user-payments-table').data('order-url')
                        );
                    }, 2000);

                }
            }
        });
    });
});

function check_boolean(val) {
    return val == 0 ? '<span class="glyphicon glyphicon-remove text-danger"></span>' : '<span class="glyphicon glyphicon-ok text-success"></span>';
}

function check_user_payments(element, payments_table, order_url) 
{
    var fetch_url = element.attr('action');
    var data = element.serialize();
    // var order_url = order_url;
    // console.log(fetch_url);
    // console.log(data);
    // console.log(order_url);
    element.find('input[type="submit"]').attr('disabled', true);
    element.find('input[type="submit"]').val('wait..');
    start_waitme2();

    $.ajax({
        url: fetch_url,
        type: 'POST',
        data: data,
        success: function(data) {
            // console.log(data);
            var tbody = payments_table;
            var str = '';
            
            if(data.payments.length > 0) {
                $.each(data.payments, function(key, value) {
                    var url = order_url.replace('order_id', value.order_id);
                    str += '<tr>';
                    str += '<td><input type="checkbox" name="payments[]" value="' + value.id + '"/></td>';
                    str += '<td>' + lexx_date_formatter(value.created_at) + '</td>';
                    str += '<td><a href="' + url + '" target="_blank">#' + value.order_id + ' <i class="fa fa-external-link"></i></a></td>';
                    str += '<td>' + value.transaction_type + '</td>';
                    str += '<td>' + value.notes + '</td>';
                    str += '<td><strong>' + accounting.formatMoney(value.amount) + '</strong></td>';
                    str += '<td>' + check_boolean(value.payment_mature_for_withdrawal) + '</td>';
                    str += '<td>' + check_boolean(value.has_tutor_withdrawn) + '</td>';
                    str += '</tr>';
                });

                $('.payments-buttons').show();
            }
            else {
                str += '<tr>';
                str += '<td colspan="5" class="text-center">No Payment Info for the user was found</td>'
                str += '</tr>';
                $('.payments-buttons').hide();
            }
            tbody.html(str);
            stop_waitme2();
            element.find('input[type="submit"]').attr('disabled', false);
            element.find('input[type="submit"]').val('Go!');  
            $('#check_all').prop('checked', false)              
        }
    });
}
    