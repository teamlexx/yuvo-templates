jQuery(document).ready(function() {
    var all_tutors_span = $('.all-tutors-span');
    var active_tutors_span = $('.active-tutors-span');
    var flagged_tutors_span = $('.flagged-tutors-span');
    var blacklisted_tutors_span = $('.blacklisted-tutors-span');
    var deactivated_tutors_span = $('.deactivated-tutors-span');
    var non_verified_tutors_span = $('.non-verified-tutors-span');

    var fetch_url = '/admin/getTutorTotals';
    var _token = $('meta[name="csrf-token"]').attr('content');

    jQuery.ajax({
        url: fetch_url,
        method: 'POST',
        data: {
            _token: _token
        },
        success: function(data) {
            // console.log(data);
            all_tutors_span.text(data.all_tutors);
            active_tutors_span.text(data.tutors_active_count);
            flagged_tutors_span.text(data.tutors_flagged_count);
            blacklisted_tutors_span.text(data.tutors_blacklisted_count);
            deactivated_tutors_span.text(data.tutors_deactivated_count);
            non_verified_tutors_span.text(data.tutors_non_verified_count);
        }
    });
    
});
