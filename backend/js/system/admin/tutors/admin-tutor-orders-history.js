$(document).ready(function() {
    var fetch_url = $('#dataTables-tutor-orders-history').data('fetch-url');
    var _token = $('meta[name="csrf-token"]').attr('content');
    var user_id = $('#dataTables-tutor-payments-history').data('user-id');

    var table = $('#dataTables-tutor-orders-history').DataTable({
        "order": [ 0, "desc" ],
        dom: 'B<lf<t>ip>',
        buttons: [
            'copy', 'excel', 'pdf'
        ],
        // searching: false,
        // paging: false,
        // info: false,
        processing: false,
        serverSide: true,
        ajax: {
            "url" : fetch_url,
            "type" : "POST",
            "data": {
                _token: _token,
                user_id: user_id
            },
        },
        columns: [
            { data: 'custom_id', name: 'custom_id' },
            { data: 'order.topic', name: 'order.topic' },
            { data: 'field', name: 'field' },
            { data: 'pages', name: 'pages'  },
            { data: 'tutor_amount', name: 'tutor_amount', 'orderable' : false, 'searchable': false },
            { data: 'custom_date', name: 'custom_date' }
        ]
    });

    // add event listener for opening and closing details
    $('#dataTables-tutor-orders-history tbody').on('click', 'tr', function() {
        var tr = $(this);
        var row = table.row(tr);

        if(row.child.isShown()) {
            // this row is already open - close it
            row.child.hide();
            tr.removeClass('shown');
        }
        else {
            // open this row
            row.child( format_child_row(row.data()) ).show();
            tr.addClass('shown');

        }
    });


});

// formatting function for row details
function format_child_row2(d) {
    // /d is he original data object for the row
    return '<div class="table-detail">' +
                '<div class="row">' +
                    '<div class="col-xs-6" style="border-right: 1px solid #fff;">' +
                        // left column entries
                        '<div class="row">' +
                            '<div class="col-xs-12">' +
                                '<p class="text-bold text-muted">Order Details</p>' +
                            '</div>' +
                            '<div class="col-xs-3 text-right">' +
                                '<p>#:</p>' +
                            '</div>' +
                            '<div class="col-xs-9 text-left">' +
                                '<p>' + d.order_id + '</p>' +
                            '</div>' +
                            '<div class="clearfix"></div>' + // clearfix

                            '<div class="col-xs-3 text-right">' +
                                '<p>Field:</p>' +
                            '</div>' +
                            '<div class="col-xs-9 text-left">' +
                                '<p>' + d.field + '</p>' +
                            '</div>' +
                            '<div class="clearfix"></div>' + // clearfix
                            
                            '<div class="col-xs-3 text-right">' +
                                '<p># of Pages:</p>' +
                            '</div>' +
                            '<div class="col-xs-9 text-left">' +
                                '<p>' + d.order.number_of_pages + '</p>' +
                            '</div>' +
                            '<div class="clearfix"></div>' + // clearfix
                            
                            '<div class="col-xs-3 text-right">' +
                                '<p># of Slides:</p>' +
                            '</div>' +
                            '<div class="col-xs-9 text-left">' +
                                '<p>' + d.order.number_of_slides + '</p>' +
                            '</div>' +
                            '<div class="clearfix"></div>' + // clearfix
                            
                        '</div>' +
                    '</div>' + // end of column 1

                    // right column entries
                    '<div class="col-xs-6">' +
                        '<div class="row">' +
                            '<div class="col-xs-12">' +
                                '<p class="text-bold text-muted">Order Preferences</p>' +
                            '</div>' +

                            '<div class="col-xs-5 text-right">' +
                                '<p>Order Academic Level:</p>' +
                            '</div>' +
                            '<div class="col-xs-7 text-left">' +
                                '<p>' + d.order.ac_level + '</p>' +
                            '</div>' +
                            '<div class="clearfix"></div>' + // clearfix
                            
                            '<div class="col-xs-5 text-right">' +
                                '<p>Work Ratings:</p>' +
                            '</div>' +
                            '<div class="col-xs-7 text-left">' +
                                '<p>-</p>' +
                            '</div>' +
                            '<div class="clearfix"></div>' + // clearfix
                            
                        '</div>' +
                    '</div>' + // end of column 2
                
                '</div>' + // end of row
            '</div>'; // end of table-details
}