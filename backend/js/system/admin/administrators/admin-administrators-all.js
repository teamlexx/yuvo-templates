$(document).ready(function() {
    var fetch_url = $('#dataTables-all-administrators').data('fetch-url');
    var _token = $('meta[name="csrf-token"]').attr('content');

    var table = $('#dataTables-all-administrators').DataTable({
        dom: 'lBfrtip',
        buttons: [
            'copyHtml5',
            'excelHtml5',
            'csvHtml5',
            'pdfHtml5'
        ],
        "order": [1, "desc"],
        // searching: false,
        // paging: false,
        // info: false,
        processing: false,
        serverSide: true,
        ajax: {
            "url" : fetch_url,
            "type" : "POST",
            "data": {
                _token: _token
            },
        },
        columns: [
            {
                "className": 'details-control',
                "orderable": false,
                "data": null,
                "defaultContent": '<strong><i class="fa fa-angle-double-down text-center"></i></strong>'
            },
            { data: 'id', name: 'id' },
            { data: 'status', name: 'status' },
            { data: 'names', name: 'names' },
            { data: 'email', name: 'email' },
            { data: 'tel', name: 'tel' },
            // { data: 'payment_email', name: 'payment_email' },
            { data: 'date_joined', name: 'date_joined' }
        ],
    });

    // add event listener for opening and closing details

    $('#dataTables-all-administrators tbody').on('click', 'tr', function() {
        var tr = $(this);
        var row = table.row(tr);

        if(row.child.isShown()) {
            // this row is already open - close it
            row.child.hide();
            tr.removeClass('shown');
        }
        else {
            // open this row
            row.child( format_child_row(row.data()) ).show();
            tr.addClass('shown');

        }
    });

});

// formatting function for row details
function format_child_row(d) {
    // /d is he original data object for the row
    return '<div class="table-detail">' +
                '<div class="row">' +
                    '<div class="col-xs-6" style="border-right: 1px solid #fff;">' +
                        // left column entries
                        '<div class="row">' +
                            '<div class="col-xs-5 text-right">' +
                                '<p>Nationality:</p>' +
                            '</div>' +
                            '<div class="col-xs-7 text-left">' +
                                '<p>' + d.nationality + '</p>' +
                            '</div>' +
                            '<div class="clearfix"></div>' + // clearfix
                            
                            '<div class="col-xs-5 text-right">' +
                                '<p>Last Login:</p>' +
                            '</div>' +
                            '<div class="col-xs-7 text-left">' +
                                '<p>' + d.last_login_at + '</p>' +
                            '</div>' +
                            '<div class="clearfix"></div>' + // clearfix
                            
                        '</div>' +
                    '</div>' + // end of column 1

                    // right column entries
                    '<div class="col-xs-6">' +
                        '<div class="row">' +
                            '<div class="col-xs-12">' +
                                '<p class="text-bold text-muted">Actions</p>' +
                            '</div>' +
                            '<div class="col-xs-12">' +
                                '<div>' + d.actions + '</div>' +
                            '</div>' +
                            '<div class="clearfix"></div>' + // clearfix
                            
                        '</div>' +
                    '</div>' + // end of column 2
                
                '</div>' + // end of row
            '</div>'; // end of table-details
}