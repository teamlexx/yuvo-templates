$(document).ready(function() {
    var fetch_url = $('#dataTables-student-payment-history').data('fetch-url');
    var student_id = $('#dataTables-student-payment-history').data('student-id');
    var _token = $('meta[name="csrf-token"]').attr('content');

    var table = $('#dataTables-student-payment-history').DataTable({
        "order": [ 0, "desc" ],
        // dom: 'B<lf<t>ip>',
        // buttons: [
        //     'copy', 'excel', 'pdf'
        // ],
        searching: false,
        // paging: false,
        // info: false, 
        processing: false,
        serverSide: true,
        ajax: {
            "url" : fetch_url,
            "type" : "POST",
            data: {
                "student_id" : student_id,
                _token: _token,
            },
        },
        columns: [
            { data: 'id', name: 'id' },
            { data: 'user_id', name: 'user_id' },
            { data: 'user_type', name: 'user_type' },
            { data: 'user_email', name: 'user_email' },
            { data: 'order_id', name: 'order_id' },
            { data: 'amount', name: 'amount' },
            { data: 'created_at', name: 'created_at' },
        ]
    });

    // add event listener for opening and closing details
    $('#dataTables-student-payment-history tbody').on('click', 'tr', function() {
        var tr = $(this);
        var row = table.row(tr);

        if(row.child.isShown()) {
            // this row is already open - close it
            row.child.hide();
            tr.removeClass('shown');
        }
        else {
            // open this row
            row.child( format_child_row(row.data()) ).show();
            tr.addClass('shown');

        }
    });

});

// check null
function check_null($str) {
    if($str == '' || $str ==  null) {
        return '-';
    } else {
        return $str;
    }
}

// formatting function for row details
function format_child_row(d) {
    // /d is he original data object for the row
    return '<div class="table-detail">' +
                '<div class="row">' +
                    '<div class="col-xs-6" style="border-right: 1px solid #fff;">' +
                        // left column entries
                        '<div class="row">' +
                            '<div class="col-xs-6 text-right">' +
                                '<p>Transaction Type:</p>' +
                            '</div>' +
                            '<div class="col-xs-6 text-left">' +
                                '<p>' + d.transaction_type  + '</p>' +
                            '</div>' +
                            '<div class="clearfix"></div>' + // clearfix
                            
                            '<div class="col-xs-6 text-right">' +
                                '<p>Payment IP:</p>' +
                            '</div>' +
                            '<div class="col-xs-6 text-left">' +
                                '<p>' + d.payment_ip  + '</p>' +
                            '</div>' +
                            '<div class="clearfix"></div>' + // clearfix

                        '</div>' +
                    '</div>' + // end of column 1

                    // right column entries
                    '<div class="col-xs-6">' +
                        '<p>Notes</p>' + check_null(d.notes) +
                    '</div>' + // end of column 2
                '</div>' + // end of row
            '</div>'; // end of table-details
}