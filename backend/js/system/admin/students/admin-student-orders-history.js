$(document).ready(function() {
    var student_id = $('#dataTables-student-orders-history').data('student-id');
    var fetch_url = $('#dataTables-student-orders-history').data('fetch-url');
    var _token = $('meta[name="csrf-token"]').attr('content');

    var table = $('#dataTables-student-orders-history').DataTable({
        "order": [ 0, "desc" ],
        processing: false,
        serverSide: true, 
        ajax: {
            "url" : fetch_url,
            "type" : "POST",
            data: {
                "student_id" : student_id,
                _token: _token,
            },
        },
        columns: [
            { data: 'custom_id', name: 'custom_id' },
            { data: 'topic', name: 'topic' },
            { data: 'field', name: 'field' },
            { data: 'pages', name: 'pages'  },
            { data: 'total_amount', name: 'total_amount', 'orderable' : false, 'searchable': false },
            { data: 'created_at', name: 'created_at' }
        ]
    });

    // add event listener for opening and closing details
    $('#dataTables-student-orders-history tbody').on('click', 'tr', function() {
        var tr = $(this);
        var row = table.row(tr);

        if(row.child.isShown()) {
            // this row is already open - close it
            row.child.hide();
            tr.removeClass('shown');
        }
        else {
            // open this row
            row.child( format_child_row2(row.data()) ).show();
            tr.addClass('shown');

        }
    });
    
});

// formatting function for row details
function format_child_row2(d) {
    // /d is he original data object for the row
    return '<div class="table-detail">' +
                '<div class="row">' +
                    '<div class="col-xs-6" style="border-right: 1px solid #fff;">' +
                        // left column entries
                        '<div class="row">' +
                            '<div class="col-xs-12">' +
                                '<p class="text-bold text-muted">Order Details</p>' +
                            '</div>' +
                            '<div class="col-xs-3 text-right">' +
                                '<p>#:</p>' +
                            '</div>' +
                            '<div class="col-xs-9 text-left">' +
                                '<p>' + d.id  + '</p>' +
                            '</div>' +
                            '<div class="clearfix"></div>' + // clearfix

                            '<div class="col-xs-3 text-right">' +
                                '<p>Topic:</p>' +
                            '</div>' +
                            '<div class="col-xs-9 text-left">' +
                                '<p>' + d.topic + '</p>' +
                            '</div>' +
                            '<div class="clearfix"></div>' + // clearfix
                            
                            '<div class="col-xs-3 text-right">' +
                                '<p>Field:</p>' +
                            '</div>' +
                            '<div class="col-xs-9 text-left">' +
                                '<p>' + d.field + '</p>' +
                            '</div>' +
                            '<div class="clearfix"></div>' + // clearfix
                            
                            '<div class="col-xs-3 text-right">' +
                                '<p># of Pages:</p>' +
                            '</div>' +
                            '<div class="col-xs-9 text-left">' +
                                '<p>' + d.number_of_pages + '</p>' +
                            '</div>' +
                            '<div class="clearfix"></div>' + // clearfix
                            
                            '<div class="col-xs-3 text-right">' +
                                '<p># of Slides:</p>' +
                            '</div>' +
                            '<div class="col-xs-9 text-left">' +
                                '<p>' + d.number_of_slides + '</p>' +
                            '</div>' +
                            '<div class="clearfix"></div>' + // clearfix
                            
                        '</div>' +
                    '</div>' + // end of column 1

                    // right column entries
                    '<div class="col-xs-6">' +
                        '<div class="row">' +
                            '<div class="col-xs-12">' +
                                '<p class="text-bold text-muted">Order Preferences</p>' +
                            '</div>' +

                            '<div class="col-xs-5 text-right">' +
                                '<p>Proposed Writer Level:</p>' +
                            '</div>' +
                            '<div class="col-xs-7 text-left">' +
                                '<p>' + d.ac_level + '</p>' +
                            '</div>' +
                            '<div class="clearfix"></div>' + // clearfix
                            
                            '<div class="col-xs-5 text-right">' +
                                '<p>Top Writer:</p>' +
                            '</div>' +
                            '<div class="col-xs-7 text-left">' +
                                '<p>' + yes_no(d.top_writer) + '</p>' +
                            '</div>' +
                            '<div class="clearfix"></div>' + // clearfix
                            
                            '<div class="col-xs-5 text-right">' +
                                '<p>High Priority:</p>' +
                            '</div>' +
                            '<div class="col-xs-7 text-left">' +
                                '<p>' + yes_no(d.high_priority) + '</p>' +
                            '</div>' +
                            '<div class="clearfix"></div>' + // clearfix
                           
                            '<div class="col-xs-5 text-right">' +
                                '<p>Quality Check:</p>' +
                            '</div>' +
                            '<div class="col-xs-7 text-left">' +
                                '<p>' + yes_no(d.quality_check) + '</p>' +
                            '</div>' +
                            '<div class="clearfix"></div>' + // clearfix
                           
                            '<div class="col-xs-5 text-right">' +
                                '<p>Deadline:</p>' +
                            '</div>' +
                            '<div class="col-xs-7 text-left">' +
                                '<p>' + d.deadline + '</p>' +
                            '</div>' +
                            '<div class="clearfix"></div>' + // clearfix
                            
                        '</div>' +
                    '</div>' + // end of column 2
                
                '</div>' + // end of row
            '</div>'; // end of table-details
}