jQuery(document).ready(function() {
    var all_students_span = $('.all-students-span');
    var active_students_span = $('.active-students-span');
    var flagged_students_span = $('.flagged-students-span');
    var blacklisted_students_span = $('.blacklisted-students-span');
    var deactivated_students_span = $('.deactivated-students-span');
    var non_verified_students_span = $('.non-verified-students-span');

    var fetch_url = '/admin/getStudentTotals';
    var _token = $('meta[name="csrf-token"]').attr('content');

    jQuery.ajax({
        url: fetch_url,
        method: 'POST',
        data: {
            _token: _token
        },
        success: function(data) {
            // console.log(data);
            all_students_span.text(data.all_students);
            active_students_span.text(data.students_active_count);
            flagged_students_span.text(data.students_flagged_count);
            blacklisted_students_span.text(data.students_blacklisted_count);
            deactivated_students_span.text(data.students_deactivated_count);
            non_verified_students_span.text(data.students_non_verified_count);
        }
    });
    
}); 
