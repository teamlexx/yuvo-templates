jQuery(document).ready(function() {
    var all_managers_span = $('.all-managers-span');
    var active_managers_span = $('.active-managers-span');
    var inactive_managers_span = $('.inactive-managers-span');
    var deactivated_managers_span = $('.deactivated-managers-span');

    var fetch_url = $('#admin-get-manager-totals').data('route'); 
    var _token = $('meta[name="csrf-token"]').attr('content');

    jQuery.ajax({
        url: fetch_url,
        method: 'POST',
        data: {
            _token: _token
        },
        success: function(data) {
            // console.log(data);
            all_managers_span.text(data.all_managers);
            active_managers_span.text(data.active_managers_span);
            inactive_managers_span.text(data.inactive_managers_span);
            deactivated_managers_span.text(data.deactivated_managers_span);
        }
    });
    
});
