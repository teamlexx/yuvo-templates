$(document).ready(function() {
    var all_orders_span = $('.all-orders-span');
    var current_orders_span = $('.current-orders-span');
    var done_orders_span = $('.done-orders-span');
    var completed_orders_span = $('.completed-orders-span');
    var revision_orders_span = $('.revision-orders-span');
    var confirmed_orders_span = $('.confirmed-orders-span');
    var rejected_orders_span = $('.rejected-orders-span');
    var disputed_orders_span = $('.disputed-orders-span');
    var available_orders_span = $('.available-orders-span');
    var pending_orders_span = $('.pending-orders-span');
    var paid_orders_span = $('.paid-orders-span');
    var unpaid_orders_span = $('.unpaid-orders-span');
    var bids_orders_span = $('.bids-orders-span');
    var bids_unconfirmed_orders_span = $('.bids-unconfirmed-orders-span');
    var urgent_orders_span = $('.urgent-orders-span');

    var fetch_url = $('#admin-get-order-totals').data('route'); 
    var _token = $('meta[name="csrf-token"]').attr('content');

    $.ajax({
        url: fetch_url,
        type: 'POST',
        data: {
            _token: _token
        },
        success: function(data) {
            // console.log("Data: " + data);
            all_orders_span.text(data.all_orders);
            current_orders_span.text(data.current_orders);
            done_orders_span.text(data.done_orders);
            completed_orders_span.text(data.completed_orders);
            confirmed_orders_span.text(data.confirmed_orders);
            revision_orders_span.text(data.revision_orders);
            rejected_orders_span.text(data.rejected_orders);
            disputed_orders_span.text(data.disputed_orders);
            available_orders_span.text(data.available_orders);
            pending_orders_span.text(data.pending_orders);
            paid_orders_span.text(data.paid_orders);
            unpaid_orders_span.text(data.unpaid_orders);
            bids_orders_span.text(data.bids_orders);
            bids_unconfirmed_orders_span.text(data.bids_unconfirmed_orders);
            urgent_orders_span.text(data.urgent_orders);
        }
    });
    
});
