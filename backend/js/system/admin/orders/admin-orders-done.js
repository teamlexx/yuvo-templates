$(document).ready(function() {
    var fetch_url = $('#dataTables-done-orders').data('fetch-url');
    var _token = $('meta[name="csrf-token"]').attr('content');

    var table = $('#dataTables-done-orders').DataTable({
        dom: 'lBfrtip',
        buttons: [
            'copyHtml5',
            'excelHtml5',
            'csvHtml5',
            'pdfHtml5'
        ],
        "order": [1, "desc"],
        processing: true,
        serverSide: true,
        ajax: {
            "url" : fetch_url, 
            "type" : "POST",
            "data": {
                _token: _token
            },
        },
        columns: [
            {
                "className": 'details-control',
                "orderable": false,
                "data": null,
                "defaultContent": '<strong><i class="fa fa-chevron-circle-down text-center"></i></strong>'
            },
            { data: 'custom_id', name: 'custom_id' },
            { data: 'order.topic', name: 'order.topic' },
            { data: 'field', name: 'field' },
            { data: 'pages', name: 'pages'  },
            { data: 'date_submitted', name: 'date_submitted' },
            { data: 'client_amount', name: 'client_amount', 'orderable' : false, 'searchable': false }
        ],
        rowCallback: function (nRow) {
            $(nRow).find('[data-countdown]').each(function () {
                var $this = $(this),
                    countdown_date = $(this).data('countdown');
                $this.countdown(countdown_date, { elapse: true })
                    .on('update.countdown', function (event) {
                        if (event.elapsed) {
                            // after end - time elapsed
                            $this.text(event.strftime('%D days %Hh %Mmin'));
                        } else {
                            // to end - time not elapsed
                            $this.text(event.strftime('%D days %Hh %Mmin'));
                        }
                    });
            });
        }
    });

    // add event listener for opening and closing details
    $('#dataTables-done-orders tbody').on('click', 'tr', function() {
        var tr = $(this);
        var row = table.row(tr);

        if(row.child.isShown()) {
            // this row is already open - close it
            row.child.hide();
            tr.removeClass('shown');
        }
        else {
            // open this row
            row.child( format_child_row(row.data()) ).show();
            tr.addClass('shown');

        }
    });

});

// formatting function for row details
function format_child_row(d) {
    // /d is he original data object for the row
    return '<div class="table-detail">' +
                '<div class="row">' +
                    '<div class="col-xs-6" style="border-right: 1px solid #fff;">' +
                        // left column entries
                        '<div class="row">' +
                            '<div class="col-xs-12">' +
                                '<p class="text-bold text-muted">Order Details</p>' +
                            '</div>' +
                            '<div class="col-xs-3 text-right">' +
                                '<p>#:</p>' +
                            '</div>' +
                            '<div class="col-xs-9 text-left">' +
                                '<p>' + d.order_id + check_order_options(d.order.high_priority, d.order.top_writer)  + '</p>' +
                            '</div>' +
                            '<div class="clearfix"></div>' + // clearfix

                            '<div class="col-xs-3 text-right">' +
                                '<p>Deadline:</p>' +
                            '</div>' +
                            '<div class="col-xs-9 text-left">' +
                                '<p>' + d.order.deadline + '</p>' +
                            '</div>' +
                            '<div class="clearfix"></div>' + // clearfix
                            
                            '<div class="col-xs-3 text-right">' +
                                '<p>Topic:</p>' +
                            '</div>' +
                            '<div class="col-xs-9 text-left">' +
                                '<p>' + d.order.topic + '</p>' +
                            '</div>' +
                            '<div class="clearfix"></div>' + // clearfix
                            
                            '<div class="col-xs-3 text-right">' +
                                '<p>Field:</p>' +
                            '</div>' +
                            '<div class="col-xs-9 text-left">' +
                                '<p>' + d.field + '</p>' +
                            '</div>' +
                            '<div class="clearfix"></div>' + // clearfix
                            
                            '<div class="col-xs-3 text-right">' +
                                '<p># of Pages:</p>' +
                            '</div>' +
                            '<div class="col-xs-9 text-left">' +
                                '<p>' + d.order.number_of_pages + '</p>' +
                            '</div>' +
                            '<div class="clearfix"></div>' + // clearfix
                            
                            '<div class="col-xs-3 text-right">' +
                                '<p># of Slides:</p>' +
                            '</div>' +
                            '<div class="col-xs-9 text-left">' +
                                '<p>' + d.order.number_of_slides + '</p>' +
                            '</div>' +
                            '<div class="clearfix"></div>' + // clearfix
                            
                            '<div class="col-xs-3 text-right">' +
                                '<p># of Charts:</p>' +
                            '</div>' +
                            '<div class="col-xs-9 text-left">' +
                                '<p>' + d.order.number_of_charts + '</p>' +
                            '</div>' +
                            '<div class="clearfix"></div>' + // clearfix

                            '<div class="col-xs-3 text-right">' +
                                '<p># of Files:</p>' +
                            '</div>' +
                            '<div class="col-xs-9 text-left">' +
                                '<p>' + d.number_of_files + '</p>' +
                            '</div>' +
                            '<div class="clearfix"></div>' + // clearfix
                            
                        '</div>' +
                    '</div>' + // end of column 1

                    // right column entries
                    '<div class="col-xs-6">' +
                        '<div class="row">' +

                            '<div class="col-xs-5">' +
                                '<p>Posted on:</p>' +
                            '</div>' +
                            '<div class="col-xs-7">' +
                                '<p>' + d.created_at  + '</p>' +
                            '</div>' +
                            '<div class="clearfix"></div>' + // clearfix

                            '<div class="col-xs-5">' +
                                '<p>Client amount:</p>' +
                            '</div>' +
                            '<div class="col-xs-7">' +
                                '<p>' + d.client_amount  + '</p>' +
                            '</div>' +
                            '<div class="clearfix"></div>' + // clearfix
                            
                            '<div class="col-xs-5">' +
                                '<p>Writer amount:</p>' +
                            '</div>' +
                            '<div class="col-xs-7">' +
                                '<p>' + d.tutor_amount  + '</p>' +
                            '</div>' +
                            '<div class="clearfix"></div>' + // clearfix
                            
                            '<div class="col-xs-5">' +
                                '<p>Writer Deadline:</p>' +
                            '</div>' +
                            '<div class="col-xs-7">' +
                                '<p>' + d.tutor_end_time  + '</p>' +
                            '</div>' +
                            '<div class="clearfix"></div>' + // clearfix                            

                            '<div class="col-xs-6">' +
                                '<p class="text-bold text-muted">Assigned Tutor ID</p>' +
                            '</div>' +
                            
                            '<div class="col-xs-6 text-left">' +
                                '<p>' + d.tutor_id  + '</p>' +
                            '</div>' +
                            '<div class="clearfix"></div>' + // clearfix
                        
                            '<div class="col-xs-12">' +
                                '<p class="text-bold text-muted">Actions</p>' +
                            '</div>' +
                            '<div class="col-xs-12">' +
                                '<div>' + d.actions + '</div>' +
                            '</div>' +
                            '<div class="clearfix"></div>' + // clearfix

                        '</div>' +
                    '</div>' + // end of column 2
                
                '</div>' + // end of row
            '</div>'; // end of table-details
}