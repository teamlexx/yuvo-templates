$(document).ready(function() {
    var fetch_url = $('#dataTables-manage-orders').data('fetch-url');
    var status = $('#dataTables-manage-orders').data('status');
    var filters = $('#dataTables-manage-orders').data('filters');
    var _token = $('meta[name="csrf-token"]').attr('content');

    var table = $('#dataTables-manage-orders').DataTable({
        dom: 'lBfrtip',
        buttons: [
            'copyHtml5',
            'excelHtml5',
            'csvHtml5',
            'pdfHtml5',
            // 'selected',
            // 'selectedSingle',
            // 'selectAll',
            // 'selectNone',
            // 'selectRows'
            // 'selectCells'
        ],
        "order": [ 1, "desc" ],
        processing: true,
        serverSide: true,
        select: {
            style: 'os',
            selector: 'td:first-child'
        },
        ajax: {
            "url" : fetch_url,
            "type" : "POST",
            "data": {
                _token: _token,
                status: status,
                filters: filters
            },
        },
        columns: [
            {
                orderable: false,
                className: 'select-checkbox',
                data: 'check',
                name: 'check'
            },
            { data: 'custom_id', name: 'custom_id' },
            { data: 'code', name: 'code' },
            { data: 'status', name: 'status' },
            { data: 'student_deadline', name: 'student_deadline' },
            { data: 'pages', name: 'pages' },
            // { data: 'client_amount', name: 'client_amount' },
            { data: 'tutor', name: 'tutor' },
            // { data: 'tutor_amount', name: 'tutor_amount'  },
            { data: 'stage', name: 'stage'  }
        ],
        columnDefs: [
            // { width: '70px', targets: 3 },
            // { width: '20px', targets: 3 },
            { width: '25%', target: 4 },
            { width: '15%', target: 7 },
        ],
        rowCallback: function(nRow) {
            $(nRow).find('[data-countdown]').each(function () {
                var $this = $(this),
                    countdown_date = $(this).data('countdown');
                $this.countdown(countdown_date, { elapse: true })
                    .on('update.countdown', function (event) {
                        if (event.elapsed) {
                            // after end - time elapsed
                            $this.text(event.strftime('%D days %Hh %Mmin'));
                        } else {
                            // to end - time not elapsed
                            $this.text(event.strftime('%D days %Hh %Mmin'));
                        }
                    });
            });
        }
    });

    // events
    var events = $('#row-results');
    table
        .on('select', function (e, dt, type, indexes) {
            var rowData = table.rows(indexes).data().toArray();
            // events.prepend('<div><b>' + type + ' selection</b> - ' + JSON.stringify(rowData) + '</div>');
            var continue_execution = true;

            // check if order is already added
            var target_div = rowData[0].id;
            var li_items = $('body #row-results li');
            li_items.each(function (key, value) {
                // console.log(value);
                var order_id = $(this).data('order-id');
                if (order_id == target_div) {
                    // order already exists
                    toastr.warning('Order already in selection');
                    continue_execution = false;
                }
            });

            if (continue_execution) {
                // continue adding order into selection
                
                var str = '<li class="list-group-item" id="row-order-' + rowData[0].id + '" data-order-id="' + rowData[0].id + '">';
                str += '<b>Order #' + rowData[0].id + '</b>';
                str += ' || Pages: <b>' + stripTags(rowData[0].pages) + '</b>';
                str += ' || Code: <b>' + stripTags(rowData[0].code) + '</b>';
                str += ' || Status: <b>' + stripTags(rowData[0].status) + '</b>';
                str += ' || Deadline: <b>' + stripTags(rowData[0].student_deadline) + '</b>';
                str += ' || Tutor Details: <b>' + stripTags(rowData[0].tutor_deadline) + '</b>';
                str += ' || Stage: <b>' + stripTags(rowData[0].stage) + '</b>';
                str += ' <span class="pull-right remove-row-result text-cursor" data-order-result="row-order-' + rowData[0].id + '"><i class="fa fa-trash fa-2x text-danger"></i> </span></b>';
                str += ' </li>';
    
                events.prepend(str);
            }
            
        })
        .on('deselect', function (e, dt, type, indexes) {
            // var rowData = table.rows(indexes).data().toArray();
            // events.prepend('<div><b>' + type + ' <i>de</i>selection</b> - ' + JSON.stringify(rowData) + '</div>');
            // events.find('#row-order-' + rowData[0].id).remove();            
        });

});

function stripTags(html) {
    var tmp = document.createElement("DIV");
    tmp.innerHTML = html;
    return tmp.textContent || tmp.innerText || "";
}

$(document).ready(function () {
    $('body').on('click', '.remove-row-result', function () {
        var targed_id = $(this).data('order-result');
        $('#' + targed_id).remove();
    });

    // clicked buttons
    $('.btn-manage-action').on('click', function () {
        var status = $(this).data('status');
        var items_selected = collectResults();

        sendAction(items_selected, status);
    });
});

function collectResults() {
    var items = [];

    var li_items = $('body #row-results li');
    li_items.each(function (key, value) {
        // console.log(value);
        var order_id = $(this).data('order-id');
        // console.log(order_id);
        items.push(order_id);
    });

    // console.log('items', items);
    return items;
}

function sendAction(items, status)
{    
    var form = $('#row-results-form');
    form.append('<input type="hidden" name="status" value="' + status + '"/>');
    form.append('<input type="hidden" name="orders" value="' + items + '"/>');
    form.trigger('submit');
}