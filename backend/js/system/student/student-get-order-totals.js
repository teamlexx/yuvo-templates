$(document).ready(function() {
    var current_orders_span = $('.current-orders-span');
    var completed_orders_span = $('.completed-orders-span');
    var disputed_orders_span = $('.disputed-orders-span');
    var cancelled_orders_span = $('.cancelled-orders-span');

    var fetch_url = '/student/getOrderTotals';
    var _token = $('meta[name="csrf-token"]').attr('content');

    $.ajax({
        url: fetch_url,
        type: 'POST',
        data: {
            _token: _token
        },
        success: function(data) {
            current_orders_span.text(data.current_orders);
            completed_orders_span.text(data.completed_orders);
            disputed_orders_span.text(data.disputed_orders);
            cancelled_orders_span.text(data.cancelled_orders);
        }
    });    
});
