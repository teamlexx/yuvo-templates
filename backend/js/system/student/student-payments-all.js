$(document).ready(function() {
    var fetch_url = $('#dataTables-all-payments').data('fetch-url');
    var _token = $('meta[name="csrf-token"]').attr('content');

    var table = $('#dataTables-all-payments').DataTable({
        "order": [ 0, "desc" ],
        dom: 'B<lf<t>ip>',
        buttons: [
            'copy', 'excel', 'pdf'
        ],
        // searching: false,
        // paging: false,
        // info: false,
        processing: true,
        serverSide: true,
        ajax: {
            "url" : fetch_url,
            "type": "POST",
            "data": {
                _token: _token
            },
        },
        columns: [
            { data: 'id', name: 'id' },
            // { data: 'user_type', name: 'user_type' },
            { data: 'order_id', name: 'order_id' },
            { data: 'user_email', name: 'user_email' },
            { data: 'amount', name: 'amount' },
            { data: 'created_at', name: 'created_at' },
            { data: 'notes', name: 'notes' },
        ]
    });

});
