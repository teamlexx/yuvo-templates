jQuery(document).ready(function() {
    var carousel = $("#howitworks-waterwheel-widget").waterwheelCarousel({
        flankingItems: 3
        
    });

    $('#howitworks-prev').bind('click', function () {
        carousel.prev();
        return false
    });

    $('#howitworks-next').bind('click', function () {
        carousel.next();
        return false;
    });

    
});