/*
    Send messages logic script in the authentication section

    if target is individual, ask for the exact recipient via ajax call

    if target is group, select the group and hit send after composing the message 

    else just go with the flow and show none;

    Wait...scratch that! that's bullshit you just read! 
*/
$(document).ready(function () {
    
    // submission form
    $('#message-form').submit(function(e) {
        e.preventDefault();
        tinymce.triggerSave();

        var submit_form = true;

        
        // finally send the damn message
        if(submit_form) {
            // tinymce.get('textarea-section').save();
            // tinymce.triggerSave();
            var data = $(this).serialize();
            var route = $(this).attr('action');

            $(this).find('#submit').prop('disabled', true);
            $(this).find('#submit').text('Sending...');
            send_message_notification(route, data);

        }
    });

    // new logic
    $('#message-form #message-form-contents input[type="radio"]').on('click', function () {
        var target = $(this).val();
        // console.log(target);
        if(target == 'individual') { 
            $('#message-form #group-section').css('display', 'none');
            $('#message-form #individual-section').fadeIn(700);
        } 
        else if(target == 'group') {
            $('#message-form #individual-section').css('display', 'none');
            $('#message-form #group-section').fadeIn(700);
        }
    });


}); 

function send_message_notification(route, data) {
    var form = $('#message-form');
    // console.log(form);
    // return;
    
    $.ajax({
        method: 'POST',
        url: route,
        data: data,
        error: function(data) {
            toastr.error('An Error Occurred. Try refreshing your browser');
        },
        success: function(data) {
            // console.log(data);
            form.find('#submit').prop('disabled', false);
            if(data.status == 'OK') {
                form.find('#submit').text('Message Sent');
                setTimeout(function() {
                    form.find('#submit').text('Send Another Message');
                }, 1000);
                form.trigger('reset');                
            } else {
                form.find('#submit').text('Retry Sending');

            }
            toastr.info(data.message);
        }
    });
}