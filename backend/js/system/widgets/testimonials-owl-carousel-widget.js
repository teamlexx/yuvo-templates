jQuery(document).ready(function() {
    $('#testimonials-owl-widget').owlCarousel({
        loop:true,
        margin:10,
        nav:false,
        navText: ['<i class="fa fa-chevron-left"></i>','<i class="fa fa-chevron-right"></i>'],
        items: 1,
        autoplay:true,
        animateOut: 'fadeOutDown',
        animateIn: 'slideInRight',

    })
});