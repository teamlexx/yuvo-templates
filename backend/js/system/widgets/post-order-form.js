/*
	LexxYungCarter Javascript order plugin. Customized and loved by the creator himself!
*/ 
function calculateOrderPrice(element, url) {
	// order form
	
	// fields-------------
	// selects: subject, type_of_work, sources, deadline, level, style, number_of_pages, language, 
	// texts: topic, preferred_writer_id, firstname, lastname, email, phone,
	// textareas: instructions
	// checkboxes: single_spaced, quality_check, high_priority, one_page_summary, top_writer, plagiarism_report, sms_updates, vip_support, bonuses, promo_code 
	// p: cost_per_page, total_price, order_summary
	// files: files[]

	var data = element.serialize();
	// disable submit button until ajax finishes its work
	element.find('input[type="submit"]').attr('disabled', true);

	start_waitme();

	// console.log(the_car_id);
	//console.log(url);
	//console.log(token);

	$.ajax({
		url: url,
		type: 'post',
		data: data,
		success: function(data) {
			// console.log(data.order_summary);
			$('#orderForm #cost_per_page').text(data.cost_per_page);
			$('#orderForm #total_price').text(data.total_cost);
			$('#orderForm #order_summary').html(data.order_summary);
			element.find('input[type="submit"]').attr('disabled', false);
            stop_waitme();
		}
	});
}
// display loading state
function start_waitme() {
	$('.waitme').waitMe({
		effect: 'pulse', // bounce, rotateplane, stretch, orbit, roundBounce, win8, win8_linear, ios, facebook, rotation, timer, pulse, progressBar, bouncePulse, img
		bg: 'rgba(255,255,255,0.1)',
		// color: rgba(255,255,255,0.9),
		text: ''
	});
}
function stop_waitme() {
	$('.waitme').waitMe('hide');
}

jQuery(document).ready(function() {

    if($('#orderForm').length) {
        var element = $('#orderForm');
        var url = element.data('route');
        var orderForm = $(this);

        // orderForm initialize on create
        calculateOrderPrice(element, url);

        // forms select on change
        $(this).on('change', 'select', function(e) {
            var select = $(this);
            // console.log(select.val());
            if (select.val() == 0) {
                select.parent().find('input').fadeIn(300);
            } else {
                select.parent().find('input').fadeOut(300);
                select.parent().find('input').val(null);
            }
            calculateOrderPrice(element, url);
        });		
        
        // forms checkbox on change
        $(this).on('change', 'input:checkbox', function(e) {
            calculateOrderPrice(element, url);
        });		

        // order submit()
        $(this).submit(function(e) {
            // first calculate the order price to ascertain no interference
            start_waitme('facebook');
            calculateOrderPrice(element, url, token);

            // check all required fields are filled
            var isValid = true;
            if(
                $('#orderForm #firstname').val() == '' ||
                $('#orderForm #laststname').val() == '' ||
                $('#orderForm #email').val() == '' ||
                // $('#orderForm #phone').val() == '' ||
                $('#orderForm #deadline').val() == '' ||
                // $('#orderForm #instructions').val() == '', 
                $('#orderForm #topic').val() == ''
            ) {
                e.preventDefault();
                isValid = false;

                // show toastr message. Import the assets on stage
                toastr.options.escapeHtml = false;
                toastr.error('<strong>You have left out some required fields!</strong> <br>Please make sure you fill out the names, email, phone, topic, and the deadline.');
            }

            if(!telInput.intlTelInput("isValidNumber")) {
                // e.preventDefault();
                // show toastr message. Import the assets on stage
                // toastr.options.escapeHtml = false;
                // toastr.error('<strong>Invalid Phone Number!</strong> <br>Please counter-check your phone number and try again.');

            }

            // redirect to other location if valid
            if(isValid) {
                // e.preventDefault();
                // var url = $(this).data('after-submit');
                // window.location = url;
            }

        });
    }

    // bootstrap number select
    var existingSetTimeout = false;
    if($('.bootstrap-spinner').length) {
        $('.bootstrap-spinner').TouchSpin({
            min:0,
            max:1000,
            step:1,
            // stepintervaldelay: 1000,
            // buttondown_class: "btn btn-danger",
            // buttonup_class: "btn btn-success",
            verticalbuttons: false
        }).on('touchspin.on.startspin', function(){
            // trigger change after 1 second
            if(existingSetTimeout) {
                // if it exists, clear it
                clearTimeout(existingSetTimeout);
            }
            // now set out a new timeout
            existingSetTimeout = setTimeout(function(){
                var element = $('#orderForm');
                var url = element.data('route');
                var orderForm = $(this);

                // orderForm initialize on create
                calculateOrderPrice(element, url);
            }, 1500);
        });
    }
    
    if($('.bootstrap-spinner-colored').length) {
        $('.bootstrap-spinner-colored').TouchSpin({
            min:0,
            max:1000,
            step:1,
            // stepintervaldelay: 1000,
            buttondown_class: "btn btn-danger",
            buttonup_class: "btn btn-success",
            verticalbuttons: false
        }).on('touchspin.on.startspin', function(){
            // trigger change after 1 second
            if(existingSetTimeout) {
                // if it exists, clear it
                clearTimeout(existingSetTimeout);
            }
            // now set out a new timeout
            existingSetTimeout = setTimeout(function(){
                var element = $('#orderForm');
                var url = element.data('route');
                var orderForm = $(this);

                // orderForm initialize on create
                calculateOrderPrice(element, url);
            }, 1500);
        });
    }

    // $('#available-orders').css({color:'#1467D2'});
    // $('.panel-group').children('.panel-default').click(function(){
    //     $('#available-orders').css({color:'#555555'});
    // });
    // $('.panel-group').children('.panel-default').mouseenter(function(){
    //     $('#available-orders').css({color:'#555555'});
    // });
//                $('#available-orders').mouseleave(function(){
//                    $(this).css({color:'#555555'});
//                })

    if($(".telInput").length) {
        // phone number validation
        var telInput = $(".telInput");
        var errorMsg = $(".error-msg");
        var validMsg = $(".valid-msg");

        // initialize plugin
        telInput.intlTelInput({
            // allowDropdown: false,
            // autoHideDialCode: false,
            // autoPlaceholder: "off",
            autoPlaceholder: true,
            // dropdownContainer: "body",
            // excludeCountries: ["us"],
            formatOnDisplay: true,
            // geoIpLookup: function(callback) {
            //   $.get("http://ipinfo.io", function() {}, "jsonp").always(function(resp) {
            //     var countryCode = (resp && resp.country) ? resp.country : "";
            //     callback(countryCode);
            //   });
            // },
            // initialCountry: "auto",
            // nationalMode: false,
            // onlyCountries: ['us', 'gb', 'ch', 'ca', 'do'],
            // placeholderNumberType: "MOBILE",
            preferredCountries: ['us', 'uk'],
            // separateDialCode: true,
            utilsScript: "/backend/plugins/intl-tel-input/build/js/utils.js"
        });
        var reset = function() {
            telInput.removeClass('error');
            errorMsg.addClass("hide");
            validMsg.addClass("hide");
        };
        // recheck phone is valid
        var recheckPhone = function() {
            if($.trim(telInput.val())) {
                if(telInput.intlTelInput("isValidNumber")) { 
                    telInput.removeClass('error');
                    validMsg.removeClass("hide");
                    errorMsg.addClass("hide");
                } else {
                    telInput.addClass("error");
                    validMsg.addClass("hide");
                    errorMsg.removeClass("hide");
                }
            }
        };
        // on blur: validate
        // telInput.on(function() {
        //  reset();
        // });
        // on keyup / change / blur flag : reset
        telInput.on("keyup change blur", function() {
            recheckPhone();
        }); 
    }

});
