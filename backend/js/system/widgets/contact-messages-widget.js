$(document).ready(function () {
    // modify subject
    $('#subject_holder').on('change', function () {
        var chosen_value = $(this).val();
        var real_subject = $('#contact-message-form').find('#real_subject');

        if (chosen_value == 'Other') {
            real_subject.val(null);
            real_subject.fadeIn(500);
        } else {
            real_subject.val(chosen_value);
            real_subject.fadeOut(500);
        }
    });

    $('#contact-message-form').submit(function(e) {
        e.preventDefault();
        
        var data = $(this).serialize();
        var url = $(this).attr('action');
        var form = $(this);
        form.find('#submit').attr('disabled', true);
        
        $.ajax({
            type: 'POST',
            method: 'POST',
            url: url,
            data: data,
            success: function(data) {
                if(data.status == 'OK') {
                    toastr.success(data.data);
                    form.find('#submit').attr('disabled', false);
                    form.trigger('reset');
                } else {
                    toastr.error(data.data);
                }
            },  
            error: function(data) {
                // console.log(data);
                toastr.error('An error occurred sending your message. Please try again or refresh the page.');
            }
        });
    });
});