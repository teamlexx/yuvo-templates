/*
	LexxYungCarter Javascript prices table widget plugin. Customized and loved by the creator himself!
*/ 
function calculateTablePrices(form, url, table, number_of_pages) {
	// order form
	
	// fields-------------
	// selects: number_of_pages, type_of_work, 

	var data = form.serialize();
	// disable submit button until ajax finishes its work
	form.find('input[type="submit"]').attr('disabled', true);

	start_waitme_win8();

	$.ajax({
		url: url,
		type: 'POST',
		data: data,
		success: function(data) {
			// console.log(data.data[0].urgency[1]);
			// console.log(number_of_pages);
			// console.log(parseFloat(data.data[1].highschool[data.urgency_columns[3]]) * number_of_pages);
            // feed thead
            var thead = '<tr>';
            thead += '<th>Urgency</th>';
            thead += '<th>Highschool</th>';
            thead += '<th>College</th>';
            thead += '<th>Undergraduate</th>';
            thead += '<th>Graduate</th>';
            thead += '<th>Master</th>';
            thead += '<th>Ph. D</th>';
            thead += '</tr>';
            table.find('thead').html(thead);

            // feed body
            var tbody = '';
            for(var i=0; i < data.data[0].urgency.length; i++) {
                tbody += '<tr>';
                tbody += '<td>' + data.data[0].urgency[i] + '</td>';
                tbody += '<td><b style="font-weight: 700; color: #aaa">$</b>' + (parseFloat(data.data[1].highschool[data.urgency_columns[i]]) * number_of_pages).toFixed(2) + '</td>';
                tbody += '<td><b style="font-weight: 700; color: #aaa">$</b>' + (parseFloat(data.data[2].college[data.urgency_columns[i]]) * number_of_pages).toFixed(2) + '</td>';
                tbody += '<td><b style="font-weight: 700; color: #aaa">$</b>' + (parseFloat(data.data[3].undergraduate[data.urgency_columns[i]]) * number_of_pages).toFixed(2) + '</td>';
                tbody += '<td><b style="font-weight: 700; color: #aaa">$</b>' + (parseFloat(data.data[4].graduate[data.urgency_columns[i]]) * number_of_pages).toFixed(2) + '</td>';
                tbody += '<td><b style="font-weight: 700; color: #aaa">$</b>' + (parseFloat(data.data[5].master[data.urgency_columns[i]]) * number_of_pages).toFixed(2) + '</td>';
                tbody += '<td><b style="font-weight: 700; color: #aaa">$</b>' + (parseFloat(data.data[6].phd[data.urgency_columns[i]]) * number_of_pages).toFixed(2) + '</td>';
                tbody += '</tr>';
            }
            table.find('tbody').html(tbody);


            stop_waitme_win8();
		},
        error: function(data) {
			console.log(data);
        }
	});
}
// display loading state
function start_waitme_win8() {
	$('.waitme-win8').waitMe({
		effect: 'win8_linear', // bounce, rotateplane, stretch, orbit, roundBounce, win8, win8_linear, ios, facebook, rotation, timer, pulse, progressBar, bouncePulse, img
		bg: 'rgba(255,255,255,0.1)',
		// color: rgba(255,255,255,0.9),
		text: 'Loading...'
	});
}
function stop_waitme_win8() {
	$('.waitme-win8').waitMe('hide');
}

jQuery(document).ready(function() {

    if($('#prices-table-widget-form').length) {
        var form = $('#prices-table-widget-form');
        var url = form.attr('action');
        var table = $('#prices-table-widget');
        var number_of_pages = form.find('input[name="number_of_pages"]').val();
        var now = form.find('#number_of_words_tally').data('number-of-words');

        // orderForm initialize on create
        calculateTablePrices(form, url, table, number_of_pages);

        // forms select on change
        form.on('change', 'select', function(e) {
            number_of_pages = form.find('input[name="number_of_pages"]').val();
            form.find('#number_of_words_tally').text(now * number_of_pages);
            calculateTablePrices(form, url, table, number_of_pages);
        });		
        
        if($('.bootstrap-spinner-sm').length) {
            existingSetTimeout = false;
            $('.bootstrap-spinner-sm').TouchSpin({
                min:1,
                max:1000,
                step:1,
                // stepintervaldelay: 1000,
                buttondown_class: "btn btn-default btn-sm",
                buttonup_class: "btn btn-default btn-sm",
                verticalbuttons: false
            }).on('touchspin.on.startspin', function(){
                // trigger change after 1 second
                if(existingSetTimeout) {
                    // if it exists, clear it
                    clearTimeout(existingSetTimeout);
                }
                // now set out a new timeout
                existingSetTimeout = setTimeout(function(){
                    number_of_pages = form.find('input[name="number_of_pages"]').val();
                    form.find('#number_of_words_tally').text(now * number_of_pages);
                    calculateTablePrices(form, url, table, number_of_pages);
                    
                }, 1500);
            });    
        }
    }

});
