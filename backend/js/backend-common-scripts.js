﻿/*
Customization Script by: Lexx YungCarter
Github: Lexx-YungCarter
Twitter: @UnderscoreLexx
Google+: google.com/+LexxYungCarter
Email: LexxYungCarter@gmail.com
==========================
This script is beautifully made with passion based on the modern approaches of JavaScript
Technologies. The items outlined here pose some critical UI and UX design patterns that should 
not be edited (unless of course, under supervision or parental guidance :-D).
Feel free to copy and paste to your projects, but on condition that you credit the author 
(of course Me, daah!)

***HAPPY CODING***

*/

// Setting up custom Laravel Notices, based on messages passed from controllers
// PNotify is beautiful, and well, Free!
var successModalTrigger = false;
var infoModalTrigger = false;
var warningModalTrigger = false;
var failModalTrigger = false;

successModalTrigger = $('#successModalTrigger').data('content');
infoModalTrigger = $('#infoModalTrigger').data('content');
warningModalTrigger = $('#warningModalTrigger').data('content');
failModalTrigger = $('#failModalTrigger').data('content');

toastr.options.closeButton = true;

// success notify
if (successModalTrigger) {
    toastr.success(successModalTrigger);
} 

// info notify
if (infoModalTrigger) {
    toastr.info(infoModalTrigger);
}

// warning notify
if (warningModalTrigger) {
    toastr.warning(warningModalTrigger);
}

// error notify
if (failModalTrigger) {
    toastr.error(failModalTrigger);
}

// get item name
function get_item_name(items_array, item_id) {
    var the_name ='';
    // console.log(items_array);
    // iterate through the array/object
    $.each(items_array, function(index, value) {
        if(parseInt(value.id) == parseInt(item_id)) {
            the_name = value.name;
        }
    }); 
    //end of iteration
    return the_name;
}

// return yes or no
function yes_no(value = 0) {
    if(value == 1) {
        return 'Yes';
    } else {
        return 'No';
    }
}

// check high_priority and top_writer
function check_order_options(high_priority = 0, top_writer = 0) {
    var str = '';

    if(high_priority == 1) {
        str += ' <span class="label label-success">Suggested</span>';
    }
    
    if(top_writer == 1) {
        str += ' <span class="label label-primary"><i class="fa fa-star-half-o"></i> Top Writer</span>';
    }
    return str; 
}

// lexx preloader 
$(document).ready(function () {
    
    // initialize datetime picker
    if($('.bothdatetimepicker').length) {
        $('.bothdatetimepicker').datetimepicker({
            // format:'Y-m-d',
            // formatDate:'Y-m-d'
        });
    }
    
    if($('.onlydatepicker').length) {
        $('.onlydatepicker').datetimepicker({
            lang: 'en',
            timepicker: false,
            format: 'd/m/Y',
            formatDate: 'Y/m/d',
            // minDate:'-1970/01/02', // yesterday is minimum date
            minDate: '-1970/01/01', // today is minimum date
            // maxDate:'+1970/01/02' // and tommorow is maximum date calendar
        });
    }

    if($('.onlytimepicker').length) {
        $('.onlytimepicker').datetimepicker({
            lang: 'en',
            datepicker: false,
            format: 'H:i',
            step: 30 
        });
    }
    
    // activate timepicker
    if ($('.flatpickr').length) {
        activate_flatpickr();
    }

    if ($('.manage-orders-datepicker').length) {
        $('.manage-orders-datepicker').datetimepicker({
            lang: 'en',
            timepicker: false,
            format: 'd/m/Y',
            formatDate: 'Y/m/d',
            maxDate:'+1970/01/01' // and today is maximum date calendar
        });
    }

    // selectize
	if($('.selectize').length) {
		selectize_selects();
    }

    // popup customization
    $('.display-popup').popover({
        trigger: 'hover',
        html: true
    });
    
    // student ratings arena
	$('.ratings-container i').on('click', function() {
		var rating = $(this).data('value');
        $('.rating-text').text(rating);
        $(this).parent().parent().find('input[name="rating"]').val(rating);
        
	});

    // Easy pie charts
    if($('.easyPieChart').length) {
        $('.easyPieChart').easyPieChart({animate: 1000});
    }

    if($('.textarea-wysihtml5').length) { 
        $('.textarea-wysihtml5').wysihtml5({
            // stylesheets: [
            //     $('#textarea-wysihtml5').attr('url') 
            //     //'vendors/bootstrap-wysihtml5-rails-b3/vendor/assets/stylesheets/bootstrap-wysihtml5/wysiwyg-color.css'
            // ]
        });
    }

    // bootstrap multi-select
    if ($('.bootstrap-multiselect').length) {
        $('.bootstrap-multiselect').multiselect();
    }
    
    // tinyMCE settings
    if($('textarea.tinymce').length) { 
        tinymce.init({
            selector : "textarea.tinymce",
            plugins : ["advlist autolink lists link image charmap print preview anchor", "searchreplace visualblocks code fullscreen", "insertdatetime media table contextmenu paste"],
            toolbar : "undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link "
        });
    }
    
    if($('textarea.tinymce-minimal').length) { 
        tinymce.init({
            selector : "textarea.tinymce-minimal",
            plugins : 'textcolor spellchecker',
            toolbar : "undo redo | bold italic | forecolor backcolor | alignleft aligncenter alignright alignjustify | spellchecker",
            menubar: false,
            statusbar: false 
        });
    }

    // show message when tr is clicked
    $('table').on('click', '.show-message', function() {
        var route = $(this).data('route');
        window.location = route;
    });

    // multiple-order countdowns
    $('body [data-countdown]').each(function() {
        var $this = $(this), countdown_date = $(this).data('countdown');
        countdown_date = $(this).data('countdown');
        $this.countdown(countdown_date, { elapse: true })
        .on('update.countdown', function(event) {
            if (event.elapsed) {
                // after end - time elapsed
                $this.text(event.strftime('%D days %Hh %Mmin'));
            } else {
                // to end - time not elapsed
                $this.text(event.strftime('%D days %Hh %Mmin'));
            }
        });
    });

    // multifile upload button
    if($('.multifile').length) {
        $('.multifile').MultiFile({
            list: '.multifile-list'
        });
    }


    // popup customization
	$('.display-popup').popover({
        trigger: 'hover',
        html: true
    });

    // bootstrap number select
    var existingSetTimeout = false;
    if ($('.bootstrap-spinner').length) {
        $('.bootstrap-spinner').TouchSpin({
            min: 0,
            max: 1000,
            step: 1,
            // stepintervaldelay: 1000,
            // buttondown_class: "btn btn-danger",
            // buttonup_class: "btn btn-success",
            verticalbuttons: false
        }).on('touchspin.on.startspin', function() {
            // trigger change after 1 second
            if (existingSetTimeout) {
                // if it exists, clear it
                clearTimeout(existingSetTimeout);
            }
            // now set out a new timeout
            existingSetTimeout = setTimeout(function() {
                var element = $('#orderForm');
                var url = element.data('route');
                var orderForm = $(this);

                // orderForm initialize on create
                calculateOrderPrice(element, url);
            }, 1500);
        });
    }

    if ($('.bootstrap-spinner-coloured').length) {
        $('.bootstrap-spinner-coloured').TouchSpin({
            min: 0,
            max: 1000,
            step: 1,
            // stepintervaldelay: 1000,
            buttondown_class: "btn btn-danger",
            buttonup_class: "btn btn-success",
            verticalbuttons: false
        }).on('touchspin.on.startspin', function() {
            // trigger change after 1 second
            if (existingSetTimeout) {
                // if it exists, clear it
                clearTimeout(existingSetTimeout);
            }
            // now set out a new timeout
            existingSetTimeout = setTimeout(function() {
                var element = $('#orderForm');
                var url = element.data('route');
                var orderForm = $(this);

                // orderForm initialize on create
                calculateOrderPrice(element, url);
            }, 1500);
        });
    }

    if ($('.telInput').length) {
        // phone number validation
        var telInput = $("#phone");
        var errorMsg = $("#error-msg");
        var validMsg = $("#valid-msg");

        // initialize plugin
        telInput.intlTelInput({
            // allowDropdown: false,
            // autoHideDialCode: false,
            // autoPlaceholder: "off",
            autoPlaceholder: true,
            // dropdownContainer: "body",
            // excludeCountries: ["us"],
            formatOnDisplay: true,
            // geoIpLookup: function(callback) {
            //   $.get("http://ipinfo.io", function() {}, "jsonp").always(function(resp) {
            //     var countryCode = (resp && resp.country) ? resp.country : "";
            //     callback(countryCode);
            //   });
            // },
            // initialCountry: "auto",
            // nationalMode: false,
            // onlyCountries: ['us', 'gb', 'ch', 'ca', 'do'],
            // placeholderNumberType: "MOBILE",
            preferredCountries: ['us', 'uk'],
            // separateDialCode: true,
            utilsScript: "/backend/plugins/intl-tel-input/build/js/utils.js"
        });
        var reset = function () {
            telInput.removeClass('error');
            errorMsg.addClass("hide");
            validMsg.addClass("hide");
        };
        // recheck phone is valid
        var recheckPhone = function () {
            if ($.trim(telInput.val())) {
                if (telInput.intlTelInput("isValidNumber")) {
                    telInput.removeClass('error');
                    validMsg.removeClass("hide");
                    errorMsg.addClass("hide");
                } else {
                    telInput.addClass("error");
                    validMsg.addClass("hide");
                    errorMsg.removeClass("hide");
                }
            }
        };
        // on blur: validate
        // telInput.on(function() {
        // 	reset();
        // });
        // on keyup / change / blur flag : reset
        telInput.on("keyup change blur", function () {
            recheckPhone();
        });

        telInput.on("countrychange", function (e, countryData) {
            // do something with countryData
            // get value of cocntry, preppend to current input
            var intlNumber = $(this).intlTelInput("getSelectedCountryData");
            var currentInput = $(this).val();
            var finalNumber = "+" + intlNumber.dialCode + "" + currentInput;
            $(this).val(finalNumber);
        });
    }


    
});

function selectize_selects() {
	// selectize
	$('.selectize').selectize();
	return ;
}

function activate_flatpickr() {
    // basic flatpickr
    if ($('.flatpickr').length) {
        $(".flatpickr").flatpickr();
    }

    // datetime
    if ($('.flatpickr.datetime').length) {
        $(".flatpickr.datetime").flatpickr(
            {
                enableTime: true,
                dateFormat: "Y-m-d H:i",
            }
        );
    }
    
    // time
    if ($('.flatpickr.time').length) {
        $(".flatpickr.time").flatpickr(
            {
                enableTime: true,
                noCalendar: true,
                dateFormat: "H:i",
            }
        );
    }
    
    // time - 24hr
    if ($('.flatpickr.time.hour-24').length) {
        $(".flatpickr.time.hour-24").flatpickr(
            {
                enableTime: true,
                noCalendar: true,
                dateFormat: "H:i",
                time_24hr: true
            }
        );
    }

    // min-date 
    if ($('.flatpickr.min-date').length) {
        var _this = $('.flatpickr.min-date');
        var mindate = _this.data('min-date');
        _this.flatpickr(
            {
                dateFormat: mindate, //"d.m.Y",
                maxDate: "15.12.2017"
            }
        );
    }
    
    // min-date today
    if ($('.flatpickr.min-date-today').length) {
        var _this = $('.flatpickr.min-date-today');
        var mindate = 'today';
        _this.flatpickr(
            {
                dateFormat: mindate, //"d.m.Y",
                maxDate: "15.12.2017"
            }
        );
    }

    // max-date
    if ($('.flatpickr.max-date').length) {
        var _this = $('.flatpickr.max-date');
        var mindate = _this.data('max-date');
        _this.flatpickr(
            {
                dateFormat: maxdate, //"d.m.Y",
                maxDate: "15.12.2017"
            }
        );
    }
    
    // date-range
    if ($('.flatpickr.date-range').length) {
        var _this = $('.flatpickr.date-range');
        var mindate = _this.data('min-date');
        var maxdate = _this.data('max-date');
        _this.flatpickr(
            {
                minDate: mindate,
                maxDate: maxdate
            }
        );
    }
    
    // max-days
    if ($('.flatpickr.max-days').length) {
        var _this = $('.flatpickr.max-days');
        var maxdays = _this.data('max-days');
        _this.flatpickr(
            {
                minDate: "today",
                maxDate: new Date().fp_incr(maxdays) // maxdays(int) days from now
            }
        );
    }
    
    // range
    if ($('.flatpickr.range').length) {
        var _this = $('.flatpickr.range');
        _this.flatpickr(
            {
                mode: "range"
            }
        );
    }

    // range - min today
    if ($('.flatpickr.range.min-today').length) {
        var _this = $('.flatpickr.range.min-today');
        var range = _this.data('range');
        _this.flatpickr(
            {
                mode: "range",
                minDate: "today",
                dateFormat: "Y-m-d",
            }
        );
    }
    
    // wrap
    if ($('.flatpickr.wrap').length) {
        var _this = $('.flatpickr.wrap');
        _this.flatpickr(
            {
                wrap: true
            }
        );
    }
}


function lexx_date_formatter(the_date) {
    if (the_date == "") {
        return null;
    }
    var m_names = new Array('Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sep','Oct','Nov','Dec');
    var formattedDate = new Date(the_date);
    var d = formattedDate.getDate();
    var m = formattedDate.getMonth();
    // m += 1; // JS are 0-11
    // no need to increment months when displaying month names
    var yr = formattedDate.getFullYear();
    return (d + " " + m_names[m] + ", " + yr);
}

$(function () {

    //Datetimepicker plugin
    if($('.datetimepicker').length) {
        $('.datetimepicker').bootstrapMaterialDatePicker({
            // format: 'dddd DD MMMM YYYY - HH:mm',
            clearButton: true,
            weekStart: 1
        });
    }

    if($('.datepicker').length) {
        $('.datepicker').bootstrapMaterialDatePicker({
            format: 'dddd DD MMMM YYYY',
            clearButton: true,
            weekStart: 1,
            time: false
        });
    }

    if($('.timepicker').length) {
        $('.timepicker').bootstrapMaterialDatePicker({
            format: 'HH:mm',
            clearButton: true,
            date: false
        });
    }

    // delete order button
    $('table').on('click', 'a.datatables-delete-btn', function(e) {
        e.preventDefault();
        url = $(this).attr('href');
        swal({
            title: "Are you sure?",
            text: "It is extremely discouraged to delete an order! It's not cool, better hold/cancel it.",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Yes, delete it!",
            cancelButtonText: "No, cancel please!",
            closeOnConfirm: false,
            closeOnCancel: false
        },
        function(isConfirm) {
            if (isConfirm) {
                swal("Deleted!", "The order has been deleted.", "success");
                window.location = url;
            } else {
                swal("Cancelled", "That was really close! :)", "error");
            }
        }); // end of swal
    });
    $('body').on('click', 'a.order-delete-btn', function(e) {
        e.preventDefault();
        url = $(this).attr('href');
        swal({
            title: "Are you sure?",
            text: "It is extremely discouraged to delete an order! It's not cool, better hold/cancel it.",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Yes, delete it!",
            cancelButtonText: "No, cancel please!",
            closeOnConfirm: false,
            closeOnCancel: false
        },
        function(isConfirm) {
            if (isConfirm) {
                swal("Deleted!", "The order has been deleted.", "success");
                window.location = url;
            } else {
                swal("Cancelled", "That was really close! :)", "error");
            }
        }); // end of swal
    });
    
    // delete user button in datatables
    $('table').on('click', 'a.datatables-delete-btn-user', function(e) {
        e.preventDefault();
        url = $(this).attr('href');
        swal({
            title: "Are you sure?",
            text: "It is extremely discouraged to delete a user! It's not cool, better flag/deactivate.",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Yes, delete it!",
            cancelButtonText: "No, cancel please!",
            closeOnConfirm: false,
            closeOnCancel: false
        },
        function(isConfirm) {
            if (isConfirm) {
                swal("Deleted!", "...Submitting Request", "success");
                window.location = url;
            } else {
                swal("Cancelled", "You almost scared the system! :)", "error");
            }
        }); // end of swal
    });

    // delete a user
    $('body').on('click', '.btn-delete-user', function(e) {
        e.preventDefault();
        url = $(this).attr('href');
        swal({
            title: "Are you sure?",
            text: "It is really hurtful letting one go!",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Yes, remove user!",
            cancelButtonText: "No, cancel please!",
            closeOnConfirm: false,
            closeOnCancel: false
        },
        function(isConfirm) {
            if (isConfirm) {
                swal("Deleted!", "...Submitting Request", "success");
                window.location = url;
            } else {
                swal("Cancelled", "That was really close! What were you thinking?! :)", "error");
            }
        }); // end of swal
    });
    
    // delete a file upload
    $('body').on('click', '.file-delete-btn', function(e) {
        e.preventDefault();
        url = $(this).attr('href');
        swal({
            title: "Wait!",
            text: "Are you sure?",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Yes, delete file!",
            cancelButtonText: "No, cancel please!",
            closeOnConfirm: false,
            closeOnCancel: false
        },
        function(isConfirm) {
            if (isConfirm) {
                swal("Deleted!", "The file has been deleted from the system.", "success");
                window.location = url;
            } else {
                swal("Cancelled", "Better check well what you click next time! :)", "error");
            }
        }); // end of swal
    });
    
    // delete a thread
    $('body').on('click', '.btn-delete-thread', function(e) {
        e.preventDefault();
        url = $(this).attr('href');
        swal({
            title: "Are you sure?",
            text: "It is extremely discouraged to delete a thread!",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Yes, delete it!",
            cancelButtonText: "No, cancel please!",
            closeOnConfirm: false,
            closeOnCancel: false
        },
        function(isConfirm) {
            if (isConfirm) {
                swal("Deleted!", "The thread has been deleted.", "success");
                window.location = url;
            } else {
                swal("Cancelled", "That was really close! What were you thinking?! :)", "error");
            }
        }); // end of swal
    });
    
    // tutor reassign/cancel order
    $('body').on('click', '.order-tutor-reassign-btn', function(e) {
        e.preventDefault();
        url = $(this).attr('href');
        swal({
            title: "Are you sure?",
            text: "Reassigning an order can attract heavy penalties to cater for any inconveniences caused",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Yes, reassign it!",
            cancelButtonText: "No, I'll do it please!",
            closeOnConfirm: false,
            closeOnCancel: false
        },
        function(isConfirm) {
            if (isConfirm) {
                swal("Reassigned!", "The order has been reassigned.", "success");
                window.location = url;
            } else {
                swal("Cancelled", "Please complete the order in time! :)", "error");
            }
        }); // end of swal
        
    });

    // order-tutor-extend-deadline-btn button
    $('.order-tutor-extend-deadline-btn').on('click', function(e) {
        $('#extend-deadline-container').toggle(100);
    });

    // form submit for checking deadline of order
    $('#extendDeadlineForm').submit(function(e) {
        e.preventDefault();
        var data = $(this).serialize();
        var url = $(this).attr('action');

        // form 1
        var submit_btn = $(this).find('input[type="submit"]');
        submit_btn.attr('disabled', true);
        submit_btn.val('Loading...');
        
        // form 2
        var submit_btn2 = $('#extendDeadlineFormConfirm').find('input[type="submit"]');
        submit_btn2.attr('disabled', true);
        submit_btn2.val('Loading...');

        start_waitme2();
        
        $.ajax({
            url: url,
            method: 'POST',
            data: data,
            success: function(results) {
                // console.log(results);
                submit_btn.attr('disabled', false);
                submit_btn.val('Check');
                submit_btn2.attr('disabled', false);
                submit_btn2.val('Confirm Deadline Extension');
                stop_waitme2();

                // feed results
                var $str = '';
                if(results.status == 'OK') {
                    $str += '<span class="glyphicon glyphicon-ok text-success"></span> ';
                    $str += '<span class="text-success">' + results.message + '</span> ';

                    // show form extendDeadlineFormConfirm
                    $('#extendDeadlineFormConfirm').show();
                    $('#extendDeadlineFormConfirm').find('#deadline').val(results.deadline);
                } else {
                    $str += '<span class="glyphicon glyphicon-remove text-danger"></span> ';
                    $str += '<span class="text-danger">' + results.message + '</span> ';
                    
                    // hide form extendDeadlineFormConfirm
                    $('#extendDeadlineFormConfirm').hide();
                }
                $('#check-deadline-extension-results').html($str);
            }
        });
    });

    // form submit for confirming deadline extension of order
    $('#extendDeadlineFormConfirm').submit(function(e) {
        e.preventDefault();
        var data = $(this).serialize();
        var url = $(this).attr('action');

        // form 1
        var submit_btn = $(this).find('input[type="submit"]');
        submit_btn.attr('disabled', true);
        submit_btn.val('Loading...');
        
        start_waitme2();
        
        $.ajax({
            url: url,
            method: 'POST',
            data: data,
            success: function(results) {
                // console.log(results);
                submit_btn.attr('disabled', false);
                submit_btn.val('Check');
                stop_waitme2();

                // feed results
                var $str = '';
                if(results.status == 'OK') {
                    // throw a toastr message
                    toastr.success('Success!' + results.message);
                    
                    // remove element
                    $('#extend-deadline-container').remove();

                    // refresh page after 5 min
                    setTimeout(function() {
                        location.reload();                        
                    }, 2000);
                } else {
                    // throw a toastr message
                    toastr.error('Error!' + results.message);
                }
            }
        });
    });
    

    // tutor' action of confirming doing an order 
    $('body').on('click', '.order-tutor-confirm-bid-btn', function(e) {
        e.preventDefault();
        url = $(this).attr('href');
        swal({
            title: "Are you sure?",
            text: "Confirm doing this order?",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Yes, I Confirm!",
            cancelButtonText: "No, I'm not sure!",
            closeOnConfirm: false,
            closeOnCancel: false
        },
        function(isConfirm) {
            if (isConfirm) {
                swal("Confirmed!", "Complete the order in time.", "success");
                window.location = url;
            } else {
                swal("Cancelled", "Please check the order and ascertain your strength doing it. :)", "error");
            }
        }); // end of swal
        
    });
    
    // universal swal with parameters intake
    $('body').on('click', '.swal-delete-btn', function(e) {
        e.preventDefault();
        var url = $(this).data('route');
        var title = $(this).data('title');
        swal({
            title: "Are you sure?",
            text: "Are you sure you want to delete this " + title + "?",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Yes, I Confirm!",
            cancelButtonText: "No, I'm not sure!",
            closeOnConfirm: false,
            closeOnCancel: false
        },
        function(isConfirm) {
            if (isConfirm) {
                swal("Confirmed!", title + " Deleted", "success");
                window.location = url;
            } else {
                swal("Cancelled", "That was close!", "error");
            }
        }); // end of swal
        
    });
    
    // tutor' action of cancelling bid in doing an order 
    $('body').on('click', '.order-tutor-cancel-bid-btn', function(e) {
        e.preventDefault();
        url = $(this).attr('href');
        var order_id = $(this).data('order-id'); // this is technically the bid_allocation_id
        $('#modalTutorCancelOrder').find('#bid_allocation_id').val(order_id);
        $('#modalTutorCancelOrder').modal('show');
    });

    // view a notification message when tr is clicked
    $('body').on('click', '.show-notification-message', function (e) {
        e.preventDefault();
        var messageid = $(this).parent().data('id');
        var messageroute = $(this).parent().data('route');
        var message = $(this).parent().data('message');
        var sender = $(this).parent().data('sender');
        var receiver = $(this).parent().data('receiver');
        var timeforhumans = $(this).parent().data('time');

        // feed to the modal
        $('#modalNotificationMessages').find('#defaultModalLabel').text('View Message Sent By #' + sender);
        $('#modalNotificationMessages').find('#modal-message').html(message);
        $('#modalNotificationMessages').find('#modal-time').text('Sent ' + timeforhumans);
        $('#modalNotificationMessages').modal('show');

        // update read count
        $.ajax({
            method: 'GET',
            url: messageroute,
            success: function (data) {
                console.log('marked as read');
            }
        });

    });

}); 

// display loading state
function start_waitme2() 
{
	$('.waitme2').waitMe({
		effect: 'win8_linear', // bounce, rotateplane, stretch, orbit, roundBounce, win8, win8_linear, ios, facebook, rotation, timer, pulse, progressBar, bouncePulse, img
		bg: 'rgba(255,255,255,0.1)',
		color: 'rgba(4,17,173,0.9)',
		text: ''
	});
}

function stop_waitme2() 
{
	$('.waitme2').waitMe('hide');
}

// display loading state
function start_waitme(type='facebook') 
{
	$('.waitme').waitMe({
		effect: type, // bounce, rotateplane, stretch, orbit, roundBounce, win8, win8_linear, ios, facebook, rotation, timer, pulse, progressBar, bouncePulse, img
		bg: 'rgba(255,255,255,0.1)',
		color: 'rgba(4,17,173,0.9)',
		text: ''
	});
}

function stop_waitme() 
{
	$('.waitme').waitMe('hide');
}

// lexx date formatter
function lexx_date_formatter(the_date) 
{
    var m_names = new Array('Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sep','Oct','Nov','Dec');
    var formattedDate = new Date(the_date);
    var d = formattedDate.getDate();
    var m = formattedDate.getMonth();
    // m += 1; // JS are 0-11
    // no need to increment months when displaying month names
    var yr = formattedDate.getFullYear();
    return (d + " " + m_names[m] + ", " + yr);
}

// generate random string via jquery
function random_string(num = 6,str = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz123456789')
{
    var text = '';
    for (var i = 0; i < num; i++) {
        text += str.charAt(Math.floor(Math.random() * str.length));
    }

    return text;
}

