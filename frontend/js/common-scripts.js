/*
Customization Script by: Lexx YungCarter
Github: Lexx-YungCarter
Twitter: @UnderscoreLexx
Google+: google.com/+LexxYungCarter
==========================
This script is beautifully made with passion based on the modern approaches of JavaScript
Technologies. The items outlined here pose some critical UI and UX design patterns that should 
not be edited (unless of course, under supervision or parental guidance :-D).
Feel free to copy and paste to your projects, but on condition that you credit the author 
(of course Me, daah!)

***HAPPY CODING***

*/

// Setting up custom Laravel Notices, based on messages passed from controllers
// PNotify is beautiful, and well, Free!
var successModalTrigger = false;
var infoModalTrigger = false;
var warningModalTrigger = false;
var failModalTrigger = false;

var notice = false;

successModalTrigger = $('#successModalTrigger').data('content');
infoModalTrigger = $('#infoModalTrigger').data('content');
warningModalTrigger = $('#warningModalTrigger').data('content');
failModalTrigger = $('#failModalTrigger').data('content');

toastr.options.closeButton = true;

// success notify
if (successModalTrigger) {
    toastr.success(successModalTrigger);
}

// info notify
if (infoModalTrigger) {
    toastr.info(infoModalTrigger);
}

// warning notify
if (warningModalTrigger) {
    toastr.warning(warningModalTrigger);
}

// error notify
if (failModalTrigger) {
    toastr.error(failModalTrigger);
}

jQuery(document).ready(function () {
    // jquery cookie policy
    cookiesPolicyBar();

    // ajax start
    jQuery(document).ajaxStart(function() {
        // jQuery('#loginform input[type=submit]').val('Loading...');
        // jQuery('#loginform input[type=submit]').css('background', '#FFA579');
    }).ajaxStop(function() {

    });

    // ajax login
    jQuery('#loginform').submit(function(b) {
        b.preventDefault();
        var email = jQuery("#loginform #email").val();
        var password = jQuery("#loginform #password").val();

        if (jQuery('#loginform #email').hasClass('has-error')) {
            jQuery('#loginform #email').removeClass('has-error');
        }

        if (jQuery('#loginform #password').hasClass('has-error')) {
            jQuery('#loginform #password').removeClass('has-error');
        }

        var form = jQuery("#loginform");
        var dataString = form.serialize();
        var formAction = form.attr('action');

        jQuery('#loginform input[type=submit]').val('Loading...');

        jQuery.ajax({
            type: "POST",
            url: formAction,
            data: dataString,
            success: function(data) {
                // console.log(data);

                if (data.auth == true) {
                    setTimeout(
                        function() {
                            jQuery('#loginform input[type=submit]').val('Login Success. Redirecting...');
                            jQuery('#loginform input[type=submit]').css('background', '#449d44');

                            setTimeout(
                                function() {
                                    window.location.href = data.intended;

                                }, 2000);

                        }, 2000);

                } else {
                    jQuery('#loginform input[type=submit]').val('Invalid! Try Again');
                    jQuery('#loginform input[type=submit]').css('background', '#FF3366');
                    setTimeout(
                        function() {
                            jQuery('#loginform input[type=submit]').val('Login Again');
                            jQuery('#loginform input[type=submit]').css('background', '#67B7E1');
                        }, 5000);
                }
            },
            error: function(data) {
                var errors = jQuery.parseJSON(data.responseText);
                console.log(errors);
            }

        }, "json");
    });

    // popup customization
    if ($('.display-popup').length) {
        $('.display-popup').popover({
            trigger: 'hover',
            html: true
        });
    }


    // selectize
    if ($('.selectize').length) {
        selectize_selects();
    }

    // bootstrap multi-select
    if ($('.bootstrap-multiselect').length) {
        $('.bootstrap-multiselect').multiselect();
    }
    
    // bootstrap password strength meter
    if ($('.bootstrap-password-strength-text').length) {
        $('.bootstrap-password-strength-text').strengthMeter('text', {
            container: $('.bootstrap-password-strength-text-container')
        });
    }
    if ($('.bootstrap-password-strength-tooltip').length) {
        $('.bootstrap-password-strength-tooltip').strengthMeter('tooltip', {
            hierarchy: {
                '0': ['text-danger', 'Think about a stronger password!'],
                '5': ['text-warning', 'Well, you are doing better ...'],
                '10': ['text-warning', 'Keep going ...'],
                '15': ['text-success', 'Ok, this will do!']
            },
            tooltip: {
                viewport: '.bootstrap-password-strength-tooltip-viewport'
            }
        });
    }
    if ($('.bootstrap-password-strength-progressBar').length) {
        $('.bootstrap-password-strength-progressBar').strengthMeter('progressBar', {
            container: $('.bootstrap-password-strength-progressBar-container'),
            hierarchy: {
                '0': 'progress-bar-danger',
                '25': 'progress-bar-warning',
                '50': 'progress-bar-success'
            }
        });
    }

    // animated wow
    if ($('.animated')) {
        wow = new WOW(
            {
                animateClass: 'animated',
                offset: 100,
                callback: function (box) {
                    // console.log("WOW: animating <" + box.tagName.toLowerCase() + ">")
                }
            }
        );
        wow.init();
    }

    // multifile upload button
    if ($('.multifile').length) {
        $('.multifile').MultiFile({
            list: '.multifile-list'
        });
    }

    // fancybox
    if ($('.fancybox').length) {
        $('.fancybox').fancybox({
            'transitionIn'	:	'elastic',
            'transitionOut'	:	'elastic',
            'speedIn'		:	600, 
            'speedOut'		:	200, 
            'overlayShow'	:	false
        });
    }

    if ($('textarea.tinymce-minimal').length) {
        tinymce.init({
            selector: "textarea.tinymce-minimal",
            plugins: 'textcolor spellchecker',
            toolbar: "undo redo | bold italic | forecolor backcolor | alignleft aligncenter alignright alignjustify | spellchecker",
            menubar: false,
            statusbar: false
        });
    }

    // bootstrap number select
    var existingSetTimeout = false;
    if ($('.bootstrap-spinner').length) {
        $('.bootstrap-spinner').TouchSpin({
            min: 0,
            max: 1000,
            step: 1,
            // stepintervaldelay: 1000,
            // buttondown_class: "btn btn-danger",
            // buttonup_class: "btn btn-success",
            verticalbuttons: false
        }).on('touchspin.on.startspin', function() {
            // trigger change after 1 second
            if (existingSetTimeout) {
                // if it exists, clear it
                clearTimeout(existingSetTimeout);
            }
            // now set out a new timeout
            existingSetTimeout = setTimeout(function() {
                var element = $('#orderForm');
                var url = element.data('route');
                var orderForm = $(this);

                // orderForm initialize on create
                calculateOrderPrice(element, url);
            }, 1500);
        });
    }

    if ($('.bootstrap-spinner-coloured').length) {
        $('.bootstrap-spinner-coloured').TouchSpin({
            min: 0,
            max: 1000,
            step: 1,
            // stepintervaldelay: 1000,
            buttondown_class: "btn btn-danger",
            buttonup_class: "btn btn-success",
            verticalbuttons: false
        }).on('touchspin.on.startspin', function() {
            // trigger change after 1 second
            if (existingSetTimeout) {
                // if it exists, clear it
                clearTimeout(existingSetTimeout);
            }
            // now set out a new timeout
            existingSetTimeout = setTimeout(function() {
                var element = $('#orderForm');
                var url = element.data('route');
                var orderForm = $(this);

                // orderForm initialize on create
                calculateOrderPrice(element, url);
            }, 1500);
        });
    }

    if ($('.telInput').length) {
        // phone number validation
        var telInput = $("#phone");
        var errorMsg = $("#error-msg");
        var validMsg = $("#valid-msg");

        // initialize plugin
        telInput.intlTelInput({
            // allowDropdown: false,
            // autoHideDialCode: false,
            // autoPlaceholder: "off",
            autoPlaceholder: true,
            // dropdownContainer: "body",
            // excludeCountries: ["us"],
            formatOnDisplay: true,
            // geoIpLookup: function(callback) {
            //   $.get("http://ipinfo.io", function() {}, "jsonp").always(function(resp) {
            //     var countryCode = (resp && resp.country) ? resp.country : "";
            //     callback(countryCode);
            //   });
            // },
            // initialCountry: "auto",
            // nationalMode: false,
            // onlyCountries: ['us', 'gb', 'ch', 'ca', 'do'],
            // placeholderNumberType: "MOBILE",
            preferredCountries: ['us', 'uk'],
            // separateDialCode: true,
            utilsScript: "/backend/plugins/intl-tel-input/build/js/utils.js"
        });
        var reset = function() {
            telInput.removeClass('error');
            errorMsg.addClass("hide");
            validMsg.addClass("hide");
        };
        // recheck phone is valid
        var recheckPhone = function() {
            if ($.trim(telInput.val())) {
                if (telInput.intlTelInput("isValidNumber")) {
                    telInput.removeClass('error');
                    validMsg.removeClass("hide");
                    errorMsg.addClass("hide");
                } else {
                    telInput.addClass("error");
                    validMsg.addClass("hide");
                    errorMsg.removeClass("hide");
                }
            }
        };
        // on blur: validate
        // telInput.on(function() {
        // 	reset();
        // });
        // on keyup / change / blur flag : reset
        telInput.on("keyup change blur", function() {
            recheckPhone();
        });

        telInput.on("countrychange", function (e, countryData) {
            // do something with countryData
            // get value of country, preppend to current input
            var intlNumber = $(this).intlTelInput("getSelectedCountryData");
            var currentInput = $(this).val();
            var finalNumber = "+" + intlNumber.dialCode + "" + currentInput;
            $(this).val(finalNumber);
        });
    }


}); // end of document ready

function selectize_selects() {
    // selectize
    $('.selectize').selectize();
    return;
}


function cookiesPolicyBar() {
    // Check cookie 
    if ($.cookie('yourCookieName') != "active") $('#cookieAcceptBar').show();
    //Assign cookie on click
    $('#cookieAcceptBarConfirm').on('click', function () {
        $.cookie('yourCookieName', 'active', { expires: 1 }); // cookie will expire in one day
        $('#cookieAcceptBar').fadeOut();
    });
}