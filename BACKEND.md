# Backend Files

## CSS
```html
<!-- awesome bootstrap checkboxes -->
https://bbcdn.githack.com/teamlexx/yuvo-templates/raw/ab0eb64f31f50d295d1cbffd5887a6984afacbbe/backend/css/awesome-bootstrap-checkbox.css

<!-- colors -->
https://bbcdn.githack.com/teamlexx/yuvo-templates/raw/ab0eb64f31f50d295d1cbffd5887a6984afacbbe/backend/css/colors.css

<!-- backend-common-styles -->
https://bbcdn.githack.com/teamlexx/yuvo-templates/raw/ab0eb64f31f50d295d1cbffd5887a6984afacbbe/backend/css/backend-common-styles.css

```

## JS
```html
<!-- backend-common-scripts -->
https://bbcdn.githack.com/teamlexx/yuvo-templates/raw/master/backend/js/backend-common-scripts.js

<!-- coolclock -->
https://bbcdn.githack.com/teamlexx/yuvo-templates/raw/ab0eb64f31f50d295d1cbffd5887a6984afacbbe/backend/js/coolclock.js

```
