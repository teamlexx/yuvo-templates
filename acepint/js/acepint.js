/*
Template Name: acepint
Description: Ace Pint
Author: Lexx YungCarter
Author URI: http://github.com/lexxyungcarter

Version: 1.0

*/

jQuery(document).ready(function () {
    // toggle read more on footer pages
    $('#footer-papers-more-toggle').on('click', function(e) {
        e.preventDefault();
        $(this).fadeOut(500).remove();
        $('#footer-papers-more').toggle(1000);
    });
});