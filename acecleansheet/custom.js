/*
Customization Script by: Lexx YungCarter
Github: Lexx-YungCarter
Twitter: @UnderscoreLexx
Google+: google.com/+LexxYungCarter
==========================
This script is beautifully made with passion based on the modern approaches of JavaScript
Technologies. The items outlined here pose some critical UI and UX design patterns that should 
not be edited (unless of course, under supervision or parental guidance :-D).
Feel free to copy and paste to your projects, but on condition that you credit the author 
(of course Me, daah!)

***HAPPY CODING***

*/

jQuery(document).ready(function() {
	// ajax start
	jQuery(document).ajaxStart(function () {
	    // jQuery('#loginform input[type=submit]').val('Loading...');
    	// jQuery('#loginform input[type=submit]').css('background', '#FFA579');
	}).ajaxStop(function () {
		
	});

	// ajax login
	jQuery('#loginform').submit(function(b) {
		b.preventDefault();
		var email=jQuery("#loginform #email").val();
		var password=jQuery("#loginform #password").val();

	    if(jQuery('#loginform #email').hasClass('has-error')){
	        jQuery('#loginform #email').removeClass('has-error');
	    }

	    if(jQuery('#loginform #password').hasClass('has-error')){
	        jQuery('#loginform #password').removeClass('has-error');
	    }

	    var form = jQuery("#loginform");
	    var dataString = form.serialize();
	    var formAction = form.attr('action');

	    jQuery('#loginform input[type=submit]').val('Loading...');
		
	    jQuery.ajax({
	        type: "POST",
	        url : formAction,
	        data : dataString,
	        success : function(data){
	            // console.log(data);

	            if (data.auth == true) {
		            setTimeout(
		                function()
		                {
		                    jQuery('#loginform input[type=submit]').val('Login Success');
		                    jQuery('#loginform input[type=submit]').css('background', '#449d44');

		                    setTimeout(
		                        function()
		                        {
		                            window.location.href = data.intended;

		                        }, 2000);

		                }, 2000);

	            } else {
        	    	jQuery('#loginform input[type=submit]').val('Invalid! Try Again');
        	    	jQuery('#loginform input[type=submit]').css('background', '#F2DEDE');
	            	setTimeout(
	            	    function()
	            	    {
		        	    	jQuery('#loginform input[type=submit]').val('Login Again');
		        	    	jQuery('#loginform input[type=submit]').css('background', '#67B7E1');
	            	    }, 5000);
	            }


	        },
	        error : function(data){
	            var errors = jQuery.parseJSON(data.responseText);
	            console.log(errors); 

	            // setTimeout(
	            //     function()
	            //     {

	            //         errorsHtml = '<div class="alert alert-danger text-center"><h4 class="margin-top-20" style="font-size: 18px;"><i class="fa fa-exclamation"></i> Anerror occurred</h4><p style="color:#35393b">Please check your details again</p><ul class="list-unstyled">';
	            //         jQuery.each( errors , function( key, value ) {
	            //             errorsHtml += '<li style="color:#35393b;">' + value[0] + '</li>'; //showing only the first error.
	            //         });
	            //         errorsHtml += '</ul></di>';

	            //         jQuery( '#form-errors' ).html( errorsHtml ); //appending to a <div id="form-errors"></div> inside form

	            //         if(errors.email){
	            //             jQuery('#loginform #email').addClass('has-error');
	            //             jQuery('#loginform #email i').css('color', '#a94442');
	            //         }

	            //         if(errors.email){
	            //             jQuery('#loginform #password').addClass('has-error');
	            //             jQuery('#loginform #password i').css('color', '#a94442');
	            //         }

	            //         jQuery('#loginform input[type=submit]').val('Reset');

	            //     }, 1500);

	        }

	    },"json");
	});
	
	// popup customization
	$('.display-popup').popover({
        trigger: 'hover',
        html: true
    });

	// selectize
	if($('.selectize').length) {
		selectize_selects();
	}

	// phone number validation
	if($('.telInput').length) {
    
        var telInput = $("#phone");
        var errorMsg = $("#error-msg");
        var validMsg = $("#valid-msg");

        // initialize plugin
        telInput.intlTelInput({
            // allowDropdown: false,
            // autoHideDialCode: false,
            // autoPlaceholder: "off",
            autoPlaceholder: true,
            // dropdownContainer: "body",
            // excludeCountries: ["us"],
            formatOnDisplay: true, 
            // geoIpLookup: function(callback) {
            //   $.get("http://ipinfo.io", function() {}, "jsonp").always(function(resp) {
            //     var countryCode = (resp && resp.country) ? resp.country : "";
            //     callback(countryCode);
            //   });
            // },
            initialCountry: "auto",
            // nationalMode: false,
            // onlyCountries: ['us', 'gb', 'ch', 'ca', 'do'],
            // placeholderNumberType: "MOBILE",
            preferredCountries: ['us', 'uk'],
            // separateDialCode: true,
            utilsScript: "/backend/plugins/intl-tel-input/build/js/utils.js"
        });
        var reset = function() {
            telInput.removeClass('error');
            errorMsg.addClass("hide");
            validMsg.addClass("hide");
        };
        // recheck phone is valid
        var recheckPhone = function() {
            if($.trim(telInput.val())) {
                if(telInput.intlTelInput("isValidNumber")) {
                    telInput.removeClass('error');
                    validMsg.removeClass("hide");
                    errorMsg.addClass("hide");
                } else {
                    telInput.addClass("error");
                    validMsg.addClass("hide");
                    errorMsg.removeClass("hide");
                }
            }
        };
        // on blur: validate
        // telInput.on(function() {
        // 	reset();
        // });
        // on keyup / change / blur flag : reset
        telInput.on("keyup change blur", function() {
            recheckPhone();
        });
    } // end of telInput	
	



}); // end of document ready

function selectize_selects() {
	// selectize
	$('.selectize').selectize();
	return ;
}

// display loading state
function start_waitme() {
	$('.waitme').waitMe({
		effect: 'facebook', // bounce, rotateplane, stretch, orbit, roundBounce, win8, win8_linear, ios, facebook, rotation, timer, pulse, progressBar, bouncePulse, img
		bg: 'rgba(255,255,255,0.1)',
		// color: rgba(255,255,255,0.9),
		text: ''
	});
}
