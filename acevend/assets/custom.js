// JavaScript Document
jQuery(document).ready(function($){
	//Put Your Custom Jquery or Javascript Code Here
	
	// resize sidebar navbar dynamically
	var windowHeight = $(window).height();
	var desiredHeight = windowHeight - 60;
	$('.vd_navbar').css('height', desiredHeight + 'px');
	$('.vd_navbar').addClass('scroll');
	$('.vd_navbar').addClass('scroll4');

	$('.slimScroll').slimScroll({
		// position: 'right',
		height: desiredHeight + 'px',
		railVisible: true,
		alwaysVisible: true
	});
});