# AceLords Yuvo Template Files
A repo for the AceLords' Freelance System template files (CSS, JS), to be delivered via [Git Hack](https://raw.githack.com/)

# GitHack CDN Links
## AcePaperstrap
CSS
```html
<!-- blocks -->
https://bbcdn.githack.com/teamlexx/yuvo-templates/raw/7a8d784e7f5acdc95c8155be0c8e0bdf9d3bba7f/acepaperstrap/css/blocks.css

<!-- core-b3 -->
https://bbcdn.githack.com/teamlexx/yuvo-templates/raw/7a8d784e7f5acdc95c8155be0c8e0bdf9d3bba7f/acepaperstrap/css/core-b3.css

<!-- default -->
https://bbcdn.githack.com/teamlexx/yuvo-templates/raw/7a8d784e7f5acdc95c8155be0c8e0bdf9d3bba7f/acepaperstrap/css/default.css

<!-- paper.bootstrap -->
https://bbcdn.githack.com/teamlexx/yuvo-templates/raw/7a8d784e7f5acdc95c8155be0c8e0bdf9d3bba7f/acepaperstrap/css/paper.bootstrap.min.css

<!-- plugins -->
https://bbcdn.githack.com/teamlexx/yuvo-templates/raw/7a8d784e7f5acdc95c8155be0c8e0bdf9d3bba7f/acepaperstrap/css/plugins.css

<!-- style-library-1 -->
https://bbcdn.githack.com/teamlexx/yuvo-templates/raw/7a8d784e7f5acdc95c8155be0c8e0bdf9d3bba7f/acepaperstrap/css/style-library-1.css

<!-- style -->
https://bbcdn.githack.com/teamlexx/yuvo-templates/raw/7a8d784e7f5acdc95c8155be0c8e0bdf9d3bba7f/acepaperstrap/css/style.css

```

JS
```html
<!-- acepaperstrap-custom -->
https://bbcdn.githack.com/teamlexx/yuvo-templates/raw/7a8d784e7f5acdc95c8155be0c8e0bdf9d3bba7f/acepaperstrap/js/acepaperstrap-custom.js

<!-- bootstrap-touch -->
https://bbcdn.githack.com/teamlexx/yuvo-templates/raw/7a8d784e7f5acdc95c8155be0c8e0bdf9d3bba7f/acepaperstrap/js/bootstrap-touch-slider.js

<!-- bskit-scripts -->
https://bbcdn.githack.com/teamlexx/yuvo-templates/raw/7a8d784e7f5acdc95c8155be0c8e0bdf9d3bba7f/acepaperstrap/js/bskit-scripts.js

<!-- ie-10 -->
https://bbcdn.githack.com/teamlexx/yuvo-templates/raw/7a8d784e7f5acdc95c8155be0c8e0bdf9d3bba7f/acepaperstrap/js/ie10-viewport-bug-workaround.js

<!-- parallaximg -->
https://bbcdn.githack.com/teamlexx/yuvo-templates/raw/7a8d784e7f5acdc95c8155be0c8e0bdf9d3bba7f/acepaperstrap/js/parallaxImg.js

<!-- plugins -->
https://bbcdn.githack.com/teamlexx/yuvo-templates/raw/7a8d784e7f5acdc95c8155be0c8e0bdf9d3bba7f/acepaperstrap/js/plugins.js

```

## Acepint
CSS
```html
<!-- main.css -->
https://bbcdn.githack.com/teamlexx/yuvo-templates/raw/06c22b9f930bfd0c3d9e0721ad2deaa989d27048/acepint/css/main.css

<!-- style.css -->
https://bbcdn.githack.com/teamlexx/yuvo-templates/raw/edc78b7cd5b325b126607d189d029c28c4878d14/acepint/css/style.css

```

JS
```html
<!-- acepint.js -->
https://bbcdn.githack.com/teamlexx/yuvo-templates/raw/06c22b9f930bfd0c3d9e0721ad2deaa989d27048/acepint/js/acepint.js

<!-- Carousel.js -->
https://bbcdn.githack.com/teamlexx/yuvo-templates/raw/06c22b9f930bfd0c3d9e0721ad2deaa989d27048/acepint/js/Carousel.js

```

## Yuvo
### For Admin Dashboard
CSS
```html
<!-- bootstrap admin theme -->
https://bbcdn.githack.com/teamlexx/yuvo-templates/raw/2e2d676f1685fd0fc14bd4738b9fa1129b37bcbc/yuvo/css/bootstrap-admin-theme.css

<!-- botstrap admin theme change size -->
https://bbcdn.githack.com/teamlexx/yuvo-templates/raw/2e2d676f1685fd0fc14bd4738b9fa1129b37bcbc/yuvo/css/bootstrap-admin-theme-change-size.css

<!-- bootstrap -->
https://bbcdn.githack.com/teamlexx/yuvo-templates/raw/2e2d676f1685fd0fc14bd4738b9fa1129b37bcbc/yuvo/css/bootstrap.css

<!-- nexus -->
https://bbcdn.githack.com/teamlexx/yuvo-templates/raw/2e2d676f1685fd0fc14bd4738b9fa1129b37bcbc/yuvo/css/nexus.css

<!-- default -->
https://bbcdn.githack.com/teamlexx/yuvo-templates/raw/2e2d676f1685fd0fc14bd4738b9fa1129b37bcbc/yuvo/css/default.css

<!-- jquery ui -->
https://bbcdn.githack.com/teamlexx/yuvo-templates/raw/2e2d676f1685fd0fc14bd4738b9fa1129b37bcbc/yuvo/css/jquery-ui.css

<!-- responsive -->
https://bbcdn.githack.com/teamlexx/yuvo-templates/raw/2e2d676f1685fd0fc14bd4738b9fa1129b37bcbc/yuvo/css/responsive.css

<!-- custom style -->
https://bbcdn.githack.com/teamlexx/yuvo-templates/raw/92d8d06a5dcc7f4eb713d54b92d87de2e69e1c9b/yuvo/css/custom-style.css

```

JS
```html
<!-- bootstrap-admin-theme-change-size -->
https://bbcdn.githack.com/teamlexx/yuvo-templates/raw/2e2d676f1685fd0fc14bd4738b9fa1129b37bcbc/yuvo/js/bootstrap-admin-theme-change-size.js

<!-- twitter bootstrap-hover -->
https://bbcdn.githack.com/teamlexx/yuvo-templates/raw/2e2d676f1685fd0fc14bd4738b9fa1129b37bcbc/yuvo/js/twitter-bootstrap-hover-dropdown.min.js

```

## AceSkyDefier
CSS
```html
<!-- bootstrap -->
https://bbcdn.githack.com/teamlexx/yuvo-templates/raw/9cedacbedc1e75212d1ed31dbd3c9a6fb5d1a20c/aceskydefier/assets/bootstrap/css/bootstrap.min.css

<!-- mobirise icons -->
https://bbcdn.githack.com/teamlexx/yuvo-templates/raw/9cedacbedc1e75212d1ed31dbd3c9a6fb5d1a20c/aceskydefier/assets/web/assets/mobirise-icons/mobirise-icons.css

https://bbcdn.githack.com/teamlexx/yuvo-templates/raw/9cedacbedc1e75212d1ed31dbd3c9a6fb5d1a20c/aceskydefier/assets/web/assets/mobirise-icons/mobirise-icons.woff

<!-- socicon/css/styles -->
<link rel="stylesheet" href="https://d1azc1qln24ryf.cloudfront.net/114779/Socicon/style-cf.css?u8vidh">

<!-- dropdown/css/style -->
https://bbcdn.githack.com/teamlexx/yuvo-templates/raw/9cedacbedc1e75212d1ed31dbd3c9a6fb5d1a20c/aceskydefier/assets/dropdown/css/style.css

<!-- theme/css/style -->
https://bbcdn.githack.com/teamlexx/yuvo-templates/raw/9cedacbedc1e75212d1ed31dbd3c9a6fb5d1a20c/aceskydefier/assets/theme/css/style.css

<!-- mobirize additional -->
https://bbcdn.githack.com/teamlexx/yuvo-templates/raw/9cedacbedc1e75212d1ed31dbd3c9a6fb5d1a20c/aceskydefier/assets/mobirise/css/mbr-additional.css

```

JS
```html
<!-- dropdown -->
https://bbcdn.githack.com/teamlexx/yuvo-templates/raw/9cedacbedc1e75212d1ed31dbd3c9a6fb5d1a20c/aceskydefier/assets/dropdown/js/script.min.js

<!-- theme -->
https://bbcdn.githack.com/teamlexx/yuvo-templates/raw/9cedacbedc1e75212d1ed31dbd3c9a6fb5d1a20c/aceskydefier/assets/theme/js/script.js

<!-- formoid -->
https://bbcdn.githack.com/teamlexx/yuvo-templates/raw/9cedacbedc1e75212d1ed31dbd3c9a6fb5d1a20c/aceskydefier/assets/formoid/formoid.min.js

```

## AceVend
CSS
```html
<!-- jquery-ui.custom -->
https://bbcdn.githack.com/teamlexx/yuvo-templates/raw/9cedacbedc1e75212d1ed31dbd3c9a6fb5d1a20c/acevend/assets/jquery-ui.custom.min.css

<!-- isotope -->
https://bbcdn.githack.com/teamlexx/yuvo-templates/raw/9cedacbedc1e75212d1ed31dbd3c9a6fb5d1a20c/acevend/assets/isotope.css

<!-- daterangepicker-bs3 -->
https://bbcdn.githack.com/teamlexx/yuvo-templates/raw/9cedacbedc1e75212d1ed31dbd3c9a6fb5d1a20c/acevend/assets/daterangepicker-bs3.css

<!-- colorpicker -->
https://bbcdn.githack.com/teamlexx/yuvo-templates/raw/9cedacbedc1e75212d1ed31dbd3c9a6fb5d1a20c/acevend/assets/colorpicker.css

<!-- theme -->
https://bbcdn.githack.com/teamlexx/yuvo-templates/raw/9cedacbedc1e75212d1ed31dbd3c9a6fb5d1a20c/acevend/assets/theme.min.css

<!-- theme-responsive -->
https://bbcdn.githack.com/teamlexx/yuvo-templates/raw/9cedacbedc1e75212d1ed31dbd3c9a6fb5d1a20c/acevend/assets/theme-responsive.min.css

<!-- custom -->
https://bbcdn.githack.com/teamlexx/yuvo-templates/raw/9cedacbedc1e75212d1ed31dbd3c9a6fb5d1a20c/acevend/assets/custom.css

```

JS
```html
<!-- modernizr -->
https://bbcdn.githack.com/teamlexx/yuvo-templates/raw/9cedacbedc1e75212d1ed31dbd3c9a6fb5d1a20c/acevend/assets/modernizr.js

<!-- mobile-detect -->
https://bbcdn.githack.com/teamlexx/yuvo-templates/raw/9cedacbedc1e75212d1ed31dbd3c9a6fb5d1a20c/acevend/assets/mobile-detect.min.js

<!-- mobile-detect-modernizr -->
https://bbcdn.githack.com/teamlexx/yuvo-templates/raw/9cedacbedc1e75212d1ed31dbd3c9a6fb5d1a20c/acevend/assets/mobile-detect-modernizr.js

<!-- jquery-ui.custom.min -->
https://bbcdn.githack.com/teamlexx/yuvo-templates/raw/9cedacbedc1e75212d1ed31dbd3c9a6fb5d1a20c/acevend/assets/jquery-ui.custom.min.js

<!-- plugins -->
https://bbcdn.githack.com/teamlexx/yuvo-templates/raw/9cedacbedc1e75212d1ed31dbd3c9a6fb5d1a20c/acevend/assets/plugins.js

<!-- breakpoints -->
https://bbcdn.githack.com/teamlexx/yuvo-templates/raw/9cedacbedc1e75212d1ed31dbd3c9a6fb5d1a20c/acevend/assets/breakpoints.js

<!-- theme -->
https://bbcdn.githack.com/teamlexx/yuvo-templates/raw/9cedacbedc1e75212d1ed31dbd3c9a6fb5d1a20c/acevend/assets/theme.js

<!-- custom -->
https://bbcdn.githack.com/teamlexx/yuvo-templates/raw/9cedacbedc1e75212d1ed31dbd3c9a6fb5d1a20c/acevend/assets/custom.js

```

